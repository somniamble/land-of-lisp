
;; let's start off simple
;; a little macro to make 'let' for one value a little nicer...

;; remember that ` puts us into quasi-quotation mode
;; , interpolates a value into the symbol
;; ,@ takes a list, and splices it into the symbol
(defmacro let1 (var val &body body) ;;&body is like &rest
  `(let ((,var ,val)) ;; do the let statement with var and val
     ,@body)) ;; splice body in


(let1 foo (+ 2 3)
  (* foo foo))
;; => 25

;; here's another syntactic form that I would like ...
(defmacro if-let (var val if-true if-nil)
  `(let ((,var ,val))
     (if ,var
	 ,if-true
	 ,if-nil)))

(if-let x '()
	"x is not nil!"
	"x is nil")
;; => "x is nil"

(if-let x (cdr '(1 2 3 4))
	(format t "The value of x is ~a" x)
	(format t "x is nil, hate to see it."))
;;(LET ((X (CDR '(1 2 3 4))))
;;  (IF X
;;      (FORMAT T "The value of x is ~a" X)
;;      (FORMAT T "x is nil, hate to see it.")))
;; => The value of x is (2 3 4)

;; WHY ARE MACROS SPECIAL?

;; macros are "basically" functions which are evaluated at compile time, rather than runtime
;;   this evaluation is expansion of the macro into a syntactic form.
;;   well there's more types of macros than just syntactic macros, but... I digress

#+nil(defun my-length (lst)
  (labels ((f (lst acc)
	     (if lst
		 (f (cdr lst) (1+ acc))
		 acc)))
    (f lst 0)))
#+nil(my-length '(1 2 3 4 5 6 7 8 9 0))
;; => 10

;;how can we make this better?

;; first draft of the split macro
#+nil (defmacro split (lst if-full if-empty)
  `(if ,lst
       (let ((head (car ,lst))
	     (tail (cdr ,lst)))
	 ,if-full)
        ,if-empty))

;; but there's a problem, since we can accidentally repeat execution of code.
#+nil (split (progn (princ "Lisp :^)")
	      '(2 3)) ;; this is a valid list
       (format t "This can be split into ~a and ~a." head tail)
       (format t "This cannot be split"))
;;(IF (PROGN (PRINC "Lisp :^)") '(2 3))
;;    (LET ((HEAD (CAR (PROGN (PRINC "Lisp :^)") '(2 3))))
;;          (TAIL (CDR (PROGN (PRINC "Lisp :^)") '(2 3)))))
;;      (FORMAT T "This can be split into ~a and ~a." HEAD TAIL)
;;      (FORMAT T "This cannot be split")))

;;; => Lisp :^)Lisp :^)Lisp :^)This can be split into 2 and (3).
;;;why does this happen?
;;; Because we passed in a (progn), which is evaluated each time we reference ,lst in the macro

;;; second draft of the split macro, using a temporary variable,
;;; is it bug-free? (no)
#+nil (defmacro split (lst if-full if-empty)
  `(let1 x ,lst
     (if x
	 (let ((head (car x))
	       (tail (cdr x)))
	   ,if-full)
	 ,if-empty)))

#+nil (split (progn (princ "Lisp :^)")
	      '(2 3)) ;; this is a valid list
       (format t "This can be split into ~a and ~a." head tail)
       (format t "This cannot be split"))
;; => Lisp :^)This can be split into 2 and (3).
;;; now let's fucking ruin it again

#+nil (let1 x 100 ;; alright so far
  (split '(2 3) ;; ok...
	 (+ x head) ;; uh oh
	 nil))
;;; fuck.
;;The value
;;  (2 3)
;;is not of type
;;  NUMBER
;;; why did this happen? because we're naming our temporary variable something that might be used already.
;; how do we fix this? (gensym) to the rescue!!

(gensym)
;; => #:G627  ;; guarenteed symbol name with no collisions

(defmacro split (val if-full if-empty)
  ;; get the symbol name, bind it to g. this seems dangerous but it's not really.
  ;; the reason being, we're evaluating this at compile time -- note that we're not quasiquoting...
  (let1 g (gensym)
    ;; now we're actually associating ,val to our (gensym) above
    `(let1 ,g ,val 
       (if ,g
	   (let ((head (car ,g))
		 (tail (cdr ,g)))
	     ,if-full)
	   ,if-empty))))

;;; let's give her another whirl
(let1 x 100         ; alright so far
  (split '(2 3)     ; ok...
	 (+ x head) ; moment of truth
	 nil))
;;; => 102  ; sick!
;;; the full macro expansion goes like this:
;;; 
;;; (LET ((X 100))
;;;   (LET ((#:G629 '(2 3)))
;;;     (IF #:G629
;;;         (LET ((HEAD (CAR #:G629)) (TAIL (CDR #:G629)))
;;;           (+ X HEAD))
;;;         NIL)))


;;; there's something we're talking around here -- Variable Capture
;;; the gensym is done to prevent variable capture in the case of the variable we're passing around internally
;;; however, we also have the 'head and 'tail variables we've created -- the user can access these,
;;; and the user can also overwrite those values
#+nil (split '(2 3)
       (let ((head "I'm a string now!"))
	 (+ 10 head))
       nil)
;;=> Value of HEAD in
;; (+ 10 HEAD)
;; is
;;   "I'm a string now!",
;; not a
;;   NUMBER.

;;; this is fine though -- 'split is an example of an ANAPHORIC MACRO --
;;;   an ANAPHORIC MACRO is a macro which creates named variables or functions (in this case 'head and 'tail)
;;;   which can be used in the body of the macro

;;; let's make another macro, a macro that let's us write recursive lambdas -- a lambda that can call itself...
(recurse (n 9)
  (if (zerop n)
      (princ "liftoff!")
      (progn (format t "~d... " n)
	     (self (1- n)))))
;; = >9... 8... 7... 6... 5... 4... 3... 2... 1... liftoff!

;; so, recurse takes some values to recur upon, and a body...

(defun pairs (lst)
  (labels ((f (lst acc)
	     (split lst
		    (if tail
			(f (cdr tail) (cons (cons head (car tail)) acc))
			(reverse acc))
		    (reverse acc))))
    (f lst nil)))

#+nil(pairs '(a b c d e f))
;;=>((A . B) (C . D) (E . f))

#+nil(pairs '(a b c))
;;=>((A . B)) ; well I don't like this...

;;; vars here is a list of arguments and their starting values
;;; I don't really like this implementation, it's not clear to me when I'd ever want to fucking use this.
(defmacro recurse (vars &body body)
  (let1 p (pairs vars)
    `(labels ((self ,(mapcar #'car p) ;; create a function 'self takes the arguments specified
		,@body))
       (self ,@(mapcar #'cdr p))))) ;; call 'self with the default values provided above

;;; I think this chapter was a little weak
;;; since this length function is completely fucking unreadable to me with the macros made
;;; but I might be spoiled bc I think of things like this in terms of reduce, bc haskell and sicp
#+nil(defun my-length (lst)
  (recurse (lst lst
		acc 0)
    (split lst
	   (self tail (1+ acc))
	   acc)))

(defun my-length (lst)
  (reduce (lambda (n _) (1+ n))
	  lst
	  :initial-value 0))
(my-length '(1 2 3 4 5 6 7 8 9 0))
;;=>10
