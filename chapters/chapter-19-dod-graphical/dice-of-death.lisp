(load "dice-of-doom-v2.lisp")
(load "server.lisp")
(load "svg.lisp")

(defparameter *green* '(15 212 63))
(defparameter *red* '(250 0 0))

(defparameter *board-size* 7)
(defparameter *board-hexnum* 49)
(defparameter *board-width* 1400)
(defparameter *board-height* 900)
(defparameter *board-scale* 64) ; radius of a single hexagon
(defparameter *top-offset* 3)   ; three extra hexes of height above the board, because of dice
(defparameter *dice-scale* 40)  ; radius of a single die
(defparameter *pip-scale* 0.10)  ; scale of the pip to a die. so a pip is 5% the size of a die.
(defparameter *pip-radius* (* *dice-scale* *pip-scale* 0.5)) ; get the radius of the pip

(defun response (document)
  (format nil "HTTP/1.1 200 OK
Content-Length: ~d
Content-Type: text/html

~a" (length document) document))


;;; x and y represent the center point of the die. color is the color... so
(defun draw-die-svg (x y color pip-color)
  (labels ((calc-pt (pt)
	     ;; translates the point by the die's center x y
	     (list (+ x (* *dice-scale* (car pt)))
		   (+ y (* *dice-scale* (cadr pt)))))
	   (f (poly color)
	     (polygon (mapcar #'calc-pt poly) color)))
    ; top rhombus
    (f '((0 -1) (-0.6  -0.75) (0 -0.5) (0.6 -0.75))
       (apply-brightness color 40))
    (f '((0 -0.5) (-0.6 -0.75) (-0.6 0) (0 0.25))
       color)
    (f '((0 -0.5) (0.6 -0.75) (0.6 0) (0 0.25))
       (apply-brightness color -40))
    (mapc (lambda (cx cy)
	    (circle (calc-pt (list cx cy)) *pip-radius* pip-color))
	  '(-0.05 0.125 0.3 -0.3 -0.125 0.05 0.2 0.2 0.45 0.45 -0.45 -0.2)
	  '(-0.875 -0.80 -0.725 -0.775 -0.70 -0.625
	    -0.35 -0.05 -0.45 -0.15 -0.45 -0.05))))
    ;(mapc (lambda (x y)
    ;	    (polygon (mapcar (lambda (xx yy)
    ;			       (calc-pt (list (+ x (* xx *dot-size*))
    ;					      (+ y (* yy *dot-size*)))))
    ;			     '(-1 -1 1 1)
    ;			     '(-1 1 1 -1))
    ;		     pip-color))
			     

#+nil
(redirect-stdout "bury-pink-die.svg" (svg 200 200
				       (draw-die-svg 50 100 *bury-pink* *nice-blue*)
				       (draw-die-svg 40 70 *bury-pink* *nice-blue*)
				       (draw-die-svg 45 40 *bury-pink* *nice-blue*)))


(defun draw-tile-svg (x y pos hex xx yy color pip-color chosen-tile)
  (loop for z below 2
	;; this draws the actual
	do (polygon (mapcar (lambda (pt)
			      (list (+ xx (* *board-scale* (car pt)))
				    (+ yy (* *board-scale*
					     (+ (cadr pt) (* (- 1 z) 0.1))))))
			    '((-1 -0.2) (0 -0.5) (1 -0.2)
			      (1 0.2) (0 0.5) (-1 0.2)))
		    (if (eql pos chosen-tile)
			(apply-brightness color 100)
			color)))
  (loop for z below (second hex)
	do (draw-die-svg (+ xx
			    (* *dice-scale*
			       0.3
			       (if (oddp (+ x y z))
					 -0.3
					 0.3)))
			 ;; offsets the y of the die by the number it is in the stack.
			 ;; remember that we want to draw the top die last, so it's drawn over the others.
			 (- yy (* *dice-scale* z 0.8)) color pip-color)))

#+nil
(redirect-stdout "nicetile.svg" (svg 300 300 (draw-tile-svg 0 0 0 '(0 3) 100 150 *bury-pink* *nice-blue* nil)))

(defparameter *tile-colors*
  (list (list *bury-pink* *nice-blue*)
	(list *nice-blue* *bury-pink*)))

(defun draw-board-svg (board chosen-tile legal-tiles)
  (loop for y below *board-size*
	do (loop for x below *board-size*
		 ;; the position of the hex within the board.
		 for pos = (+ x (* *board-size* y))
		 ;; the actual hexagon containing (<player> <num-dice>)
		 for hex = (aref board pos)
		 ;; i am pretty sure this is generating the center points of each tile 
		 for xx = (* *board-scale* (+ (* 2 x) (- *board-size* y)))
		 for yy = (* *board-scale* (+ (* y 0.7) *top-offset*))
		 ;; the math here for the brightness is darkening the tiles in the back a bit... 
		 ;; colors has: tile color in the front,
		 for colors = (mapcar (lambda (c) (apply-brightness c (* -15 (- *board-size* y))))
				      (nth (first hex) *tile-colors*))
		 do (if (or (member pos legal-tiles) (eql pos chosen-tile))
			(tag g ()
			  (tag a ("xlink:href" (make-game-link pos))
			    (draw-tile-svg x y pos hex xx yy (car colors) (cadr colors) chosen-tile)))
			(draw-tile-svg x y pos hex xx yy (car colors) (cadr colors) chosen-tile)))))


(defun make-game-link (pos)
  (format nil "/game.html?chosen=~a" pos))

;;(redirect-stdout "this-is-a-nice-board.svg"
;)
;;(svg *board-width* *board-height* (draw-board-svg (cadr (new-game)) 0 nil))

;;; the current game tree
(defparameter *cur-game-tree* nil)

;;; the currently selected tile I think
(defparameter *from-tile* nil)

(defun dod-request-handler (path header params)
  ;; horrible.
  (princ
  (response
   (with-output-to-string (*standard-output*)
	(if (equal path "game.html")
	    (html
		(head "Dice of Doom!")
		(body
		(tag center ()
		    (tag h1 () "Welcome to DICE OF DOOM")
		    (etag br ())
		    (let ((chosen (assoc 'chosen params)))
		    ;; if the game tree isn't initialized / if the player hasn't chosen a tile,
		    ;; that mean's we're starting up a new game.
		    (when (or (not *cur-game-tree*) (not chosen))
			(setf chosen nil)
			(web-initialize))
			    ;; if the game has ended
		    (cond ((lazy-null (caddr *cur-game-tree*))
			    ;; announce the winner
			    (web-announce-winner (cadr *cur-game-tree*)))
			    ;; if the player is 0, that means it's human's turn
			    ((zerop (car *cur-game-tree*))
			    (web-handle-human
			    (when chosen
				(read-from-string (cdr chosen)))))
			    (t (web-handle-computer))))
		    (etag br ())
		    (princ (draw-dod-page *cur-game-tree* *from-tile*)))))
	    (html
		(head "404")
		(body
		(tag h1 () (princ "this page doesn't exist :("))
		(tag a (href "game.html") (princ "return to safety?")))))))))

(defun web-initialize ()
  (setf *from-tile* nil)
  (setf *cur-game-tree* (new-game)))

(defun web-announce-winner (board)
  (fresh-line)
  (let ((w (winners board)))
    (if (> (length w) 1)
	(format t "The game is a tie between ~a ~{and ~a~}"
		(player-letter (car w)) (mapcar #'player-letter (cdr w)))
	(format t "The winner is ~a" (player-letter (car w)))))
  (tag a (href "game.html")
    (princ "Play again?")))

(defun web-handle-human (pos)
  (cond ((not pos) (princ "Please choose a hex to move from:"))
	((eq pos 'pass) (setf *cur-game-tree*
			      ;; update the current game tree to the passing game tree.
			      (cadr (lazy-car (caddr *cur-game-tree*))))
	 (princ "Your reinforcements have been placed.")
	 (tag a (href (make-game-link nil))
	   (princ "continue")))
	;; this is the first tile we're selecting, so *from-tile* is false. update it with our position
	((not *from-tile*) (setf *from-tile* pos)
	 (princ "Now make your attack:"))
	((eq pos *from-tile*) (setf *from-tile* nil)
	 (princ "Move cancelled."))
	;; apply the new game tree that we move to
	(t (setf *cur-game-tree*
		 (cadr (lazy-find-if (lambda (move)
				       (equal (car move)
					      ;; search for the move described by (*from-tile* pos)
					      (list *from-tile* pos)))
				     (caddr *cur-game-tree*))))
	   ;; update from-tile to nil, since we made our move
	   (setf *from-tile* nil)
	   (princ "You may now ")
	   (tag a (href (make-game-link 'pass))
	     (princ "pass"))
	   (princ " or make another move:"))))
	

(defun web-handle-computer ()
  (setf *cur-game-tree* (handle-computer *cur-game-tree*))
  (princ "The computer has moved")
  (tag script ()
    (princ
     "window.setTimeout('window.location=\"game.html?chosen=NIL\"',1000)")))

(defun draw-dod-page (tree selected-tile)
  (svg *board-width* *board-height*
    (draw-board-svg (cadr tree)
		    selected-tile
		     (take-all (if selected-tile
				   ;; gets the list of destinations from moves starting with selected-tile
				   (lazy-mapcar
				    (lambda (move)
				      (when (eql (caar move)
						 selected-tile)
					(cadar move)))
				    (caddr tree))
				   ;; gets the starting positions from all moves currently avaiable
				   (lazy-mapcar #'caar (caddr tree)))))))
