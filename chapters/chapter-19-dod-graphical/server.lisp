(require 'usocket)

(defun split-string (s on-char)
  (let ((idx (position on-char s)))
    (when idx
      (cons (subseq s 0 idx)
	     (subseq s (1+ idx))))))

(defun trim-whitespace (s)
  (string-trim '(#\Space #\Tab #\Newline #\Linefeed) s))

(defun to-symbol (s)
  (intern (string-upcase s)))

		     
(defun http-char (c1 c2 &optional (default #\Space))
  (let ((code (parse-integer                 ;; parse and integer from...
	       (coerce (list c1 c2) 'string) ;; the stringified version of '(c1 c2)...
	       :radix 16                     ;; in hexadecimal form...
	       :junk-allowed t)))            ;; permitting junk
    (if code
	(code-char code) ;; code-char takes a character code and returns the corresponding char, or nil if none
	default))) 

;; decode-param takes the query string and turns it into a decoded literal string
(defun decode-param (s)
  (labels ((f (lst) ;; f is a recursive function which consumes the entire list given to it
	     (when lst ;; if there's anything left in the list
	       (case (car lst) ;; switch on car
		 (#\% (cons (http-char (cadr lst) (caddr lst)) ;; decode the percent encoded character
			    (f (cdddr lst))))                  ;; cons it on to (f rest-of-list)
		 (#\+ (cons #\space (f (cdr lst))))            ;; decode a #\+ as a space
		 (otherwise (cons (car lst) (f (cdr lst))))))));; otherwise the character is fine, continue
    (coerce (f (coerce s 'list)) 'string))) ;; coerce starting string to list, (f lst), then coerce back to string

;;(decode-param "foo")
;;;;=>"foo"
;;(decode-param "this+is+a%20test")
;;;; => "this is a test"
;;(decode-param "%3C%3D%3D%3Doh%21hell%3Fyeah%7E%3D%3D%3D%3E")
;;;; => "<===oh!hell?yeah~===>"

;;(let* ((str "name=bob")
;;       (i1 (position #\= str))      ;; 4
;;       (i2 (position #\& str)))     ;; nil
;;   (cons (intern (subseq str 0 i1)) ;; (subseq "name=bob" 0 4) => "name"
;;         (subseq str (1+ i1) i2)))  ;; (subseq "name=bob" 5 nil) => "bob", nil is treated as "until end"
;; => (|name| . "bob")
(defun parse-params (s)
  (let* ((idx-amp (position #\& s))
	 (splitted (split-string (subseq s 0 idx-amp) #\=)))
    ;; the first cons is adding onto a list, the second cons is making an alist pair
    (cond (splitted (cons (cons (to-symbol (car splitted))
				(decode-param (cdr splitted)))
		     (and idx-amp (parse-params (subseq s (1+ idx-amp))))))
    ;;(cond (i1 (cons (split-string
	       ;;(cons (intern (string-upcase (subseq s 0 i1))) ;; intern produces a |symbol| from "symbol"
	       ;;		  (decode-param (subseq s (1+ i1) i2))) 
	       ;;	    (and i2 (parse-params (subseq s (1+ i2)))))) ;; `and` short-circuits on nil
	       ;;           ;; recursively parse out the rest of the string after the #\&
	  ((equal s "") nil)
	  (t s))))

;;(parse-params "name=bob")
;; => ((NAME . "bob"))
(parse-params "name=bob&vinny=vinesauce&ok=go")
;; => ((NAME . "bob") (VINNY . "vinesauce") (OK . "go"))
		    

;; parses the url out of the first line of an HTTP request, as well as any parameters
(defun parse-url (s)
  (let* ((url (subseq s
		      (+ 2 (position #\space s))
		      (position #\space s :from-end t)))
	 (x (position #\? url))) ;; if a #\? is present that indicates we have a query string
    (if x
	(cons (subseq url 0 x) (parse-params (subseq url (1+ x))))
	(cons url '()))))

;;(parse-url "POST /login.html HTTP/1.1")
;; => ("login.html")

;;(parse-url "GET /lolcats.html?extranasty=yes%20please HTTP/1.1")
;; => ("lolcats.html" (EXTRANASTY . "yes please"))

;; here's where we get the rest of the request headers
(defun get-header (stream)
  (let* ((s (read-line stream)) ;; s is a line read from the stream, which advances the position in the stream...
	 (h (split-string s #\: )))
    (when h
      (cons (cons (to-symbol (car h)) (trim-whitespace (cdr h)))
	    (get-header stream)))))

;; (get-header (make-string-input-stream "foo: 1
;; bar: abc, 123
;; 
;; "))
;; => ((|foo| . "1") (|bar| . "abc, 123"))

;; given a stream and headers, attempts to parse out the body into parameters
(defun get-content-params (stream headers)
  (let ((content-length (cdr (assoc 'CONTENT-LENGTH headers)))) ;; gets the content length header
    (when content-length ;; only run this when we actually have a content-length header
      (let ((content (make-string (parse-integer content-length)))) ;; make-string creates a string of given length
	(read-sequence content stream) ;; read-sequence reads from a stream and puts into a sequence 
	(parse-params content))))) ;; parse out parameters from the content
                                   ;; we could just as easily parse out here into json or whatever.


(let* ((string "Accept-Language: en-us,en;q=0.5
Accept-Encoding: gzip,deflate
Accept-Charset: ISO-8859,utf-8,q=0.7.*;q=0.7
Keep-Alive: 300
Connection: keep-alive
Content-Length: 39

userid=foo&password=supersecretpassword")
       (stream (make-string-input-stream string))
       (headers (get-header stream)))
  ;;headers)
;; => ((ACCEPT-LANGUAGE . "en-us,en;q=0.5") (ACCEPT-ENCODING . "gzip,deflate")
;; (ACCEPT-CHARSET . "ISO-8859,utf-8,q=0.7.*;q=0.7") (KEEP-ALIVE . "300")
;; (CONNECTION . "keep-alive") (CONTENT-LENGTH . "39"))
  (get-content-params stream headers))
;;=>((USERID . "foo") (PASSWORD . "supersecretpassword"))

(defun serve (request-handler)
  (let ((socket (usocket:socket-listen #(127 0 0 1) 8081))) ;; create a socket bound to 8080 (i.e. socket create, socket bind, socket listen)
    (unwind-protect ;; run the entire thing within an unwind-protect, to ensure we clean the socket up
	 (loop (with-open-stream (stream (usocket:socket-stream (usocket:socket-accept socket)))  ;; block and start accepting incoming connections. Create a socket stream when a client appears.
		 (let* ((url    (parse-url (read-line stream))) ;; parse the url from the stream, path and params
			(path   (car url))                      ;; extract the path from the url
			(header (get-header stream))            ;; extract headers from the stream
			(params (append (cdr url)               ;; extract params from both the url and from the request body
					(get-content-params stream header)))
			;;(*standard-output* stream)             ;; point *standard-output* to our stream, so we can just print/c stuff out
			(response (funcall request-handler path header params))) ;; call the request handler, passing in path, headers, params
;;		   (prin1 response)
	       (princ response stream))))
      (usocket:socket-close socket)))) ;; close the socket up
