(require 'usocket)

;; socket-listen indicates this is our listener socket
(defparameter my-socket (usocket:socket-listen #(127 0 0 1) 5432))

;; this is a blocking operation
;; we are basically waiting until a client connects, you see
(defparameter my-stream (usocket:socket-stream
			 (usocket:socket-accept my-socket)))

(read my-stream)

;; respond and force the buffer onto the stream
(print "Ahaa suuuuh dUDE" my-stream)
(force-output my-stream)

;; close the streamb.
(close my-stream)
