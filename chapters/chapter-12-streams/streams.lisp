;; It is at work everywhere, functioning smoothly at times
;; at other times in fits and starts. It breathes, it heats,
;; it eats. It shits and fucks. What a mistake to have ever said
;; THE id. Everywhere IT is machines -- real ones, not figurative ones;
;; machines driving other machines, machines being driven by other machines,
;; with all the necessary couplings and connections.
;; An organ machine is plugged into an energy-source-machine:
;; the one produces a flow that the other interrupts.
;;
;; Anti-Oedipus, D+G


;; streams are how we communicate with the external world in lisp
;; there are a number of different streams:
;; console streams -- basically stdin/stdout, a special kind of file stream
;; file streams -- read/write to disk
;; socket streams -- communicate with other computers on a network!!
;; string streams -- send and receive text from a lisp string. esoteric.

;; Streams have directionality:
;; when you write data to a resource, you use `output streams`.
;; when you read data from a resource, you use `input streams`.

;;Output Streams
;; At the most primitive level, you can do two things to an output stream:
;; - Check if it's valid
;; - Push a new item onto the stream

;; A stream is more restrictive than other kinds of data types in lisp.... but this is a good thing!
;; Let's see if *standard-output* is a valid output stream...
(output-stream-p *standard-output*)
;; => T

;; Let's try writing a character-- that is,
;; let's try pushing a character onto an output stream!
(write-char #\x *standard-output*)
;; => x#\x ;; printed out x, and also returned '#\x'

(write-string "The Sky And The Cosmos Are One" *standard-output*)
;; => The Sky And The Cosmos Are One"The Sky And The Cosmos Are One"

;; squaks at us if we do this.
;;(write-string "The Sky And The Cosmos Are One" *standard-input*)

;; This chapter is only covering text streams.

;; Let's check if *standard-input* is a valid input stream
(input-stream-p *standard-input*)
;; => T

;; let's try reading from *standard-input*
(read-char *standard-input*)
;; >123
;; => #\1 ;; the first character was popped off the stream and returned

;; use `print` to communicate with *standard-output*
(print 'Suh-Dude *standard-output*)
;;=> SUH-DUDE SUH-DUDE

;; WORKING WITH FILES

;; the best way to open files in common lisp is with-open-file, it prevents us from fucking up :)
;; the (my-stream "data.txt" :direction :output) expression is similar to a `let` or `labels` expression
;; the stream, or file-handle, is given to us with given directionality (:output)
;; it refers to the file with relative path "data.txt",
;; and we refer to it with the name `my-stream`
;; with-open-file is a standard binding ritual
(with-open-file (my-stream "data.txt" :direction :output)
  (print "The Sky And The Cosmos Are One" my-stream))
;; =>"The Sky And The Cosmos Are One"

;; similar to before, but this time it's an input stream
(with-open-file (my-stream "data.txt" :direction :input)
  (read my-stream))
;;=>"The Sky And The Cosmos Are One"


;; we can use common lisp to write datatypes to a file and pull them back out...
(let ((lemur-noises '((kattak . snarlsnap)
		       (oddubb . blubblubb)
		      (murmur . bloop)
		      (djynxx . zuzuzuzuzuzuzuzuzuzu)
		      (uttunul . ---))))
  (with-open-file (my-stream "lemur-noises.txt" :direction :output)
    (prin1 lemur-noises my-stream)))
;;=>((KATTAK . SNARLSNAP) (ODDUBB . BLUBBLUBB) (MURMUR . BLOOP) \
;;  (DJYNXX . ZUZUZUZUZUZUZUZUZUZU) (UTTUNUL . ---))

(with-open-file (my-stream "lemur-noises.txt" :direction :input)
  (read my-stream))
;;=>((KATTAK . SNARLSNAP) (ODDUBB . BLUBBLUBB) (MURMUR . BLOOP) \
;; (DJYNXX . ZUZUZUZUZUZUZUZUZUZU) (UTTUNUL . ---))

(with-open-file (my-stream "data.txt" :direction :output :if-exists :error)
  (print "my data" my-stream))
;; *** file already exists

(with-open-file (my-stream "data.txt" :direction :output
			              :if-exists :supersede)
  (prin1 (* 19 37 (+ 1000000 (random 1000000))) my-stream))

;; here's what a with-open-file call macro-expands to...
;; common lisp has many different `with-` macros, which handle resources
;; and will gracefully close them even if the code throws
(with-open-file (my-stream "hell.........yes.txt" :direction :output
						  :if-exists :supersede)
  (prin1 "THE SKY IS DARK THE WIND IS COLD THE NIGHT IS YOUNG BEFORE ITS OLD AND GRAY. WE WILL KNOW THE THRILL OF IT ALL--BYRAN FREEY"))
;;(LET ((MY-STREAM
;;       (OPEN "hell.........yes.txt" :DIRECTION :OUTPUT :IF-EXISTS :SUPERSEDE))
;;      (#:G634 T))
;;  (UNWIND-PROTECT
;;      (MULTIPLE-VALUE-PROG1
;;          (PROGN
;;           (PRIN1
;;            "THE SKY IS DARK THE WIND IS COLD THE NIGHT IS YOUNG BEFORE ITS OLD AND GRAY. WE WILL KNOW THE THRILL OF IT ALL--BYRAN FREEY"))
;;        (SETQ #:G634 NIL))
;;    (WHEN MY-STREAM (CLOSE MY-STREAM :ABORT #:G634))))

;; STRING STREAMS LETS GOOOOOOOOOOOOOOOOOOO

(defparameter foo (make-string-output-stream))
(princ "This is food for foo. " foo)
(princ "And this too shall pass." foo)
(get-output-stream-string foo)
;; => "This is food for foo. And this too shall pass."

;; string streams can be used many places a say, a file stream could
;; you can also use it with something like, concatenating many strings together
;; similar to stringbuilder functionality in C# or stringstream shit in C++

;; you can also do this nauseating fucking hack
(with-output-to-string (*standard-output*)
  (princ "The sum of ")
  (princ 5)
  (princ " and ")
  (princ 4)
  (princ " is ")
  (princ (+ 5 4)))
;; => "The sum of 5 and 4 is 9"
  
