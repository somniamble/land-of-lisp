;; as you read this chapter, remember that handling text is not a computer's strength.
;; It is a necessary evil best kept to a minimum.

;;;; WIZARD'S APPRENTICE ;;;;
;; LIVING ROOM <-- door --> GARDEN
;; LIVING ROOM <-- ladder --> ATTIC

;; our code will need to handle:
;; - Looking around
;; - Walking to different locations
;; - picking up objects
;; - performing actions on the objects picked up

;; when looking around, we will be able to "see" three kinds of things from a location
;; - Basic scenery
;; - One or more paths to other locations
;; - Objects that you can pick up and manipulate

;; node structure:
;; (<location> (<description>))

;; edge structure:
;; (<location> (<location> <direction> <via>))

;; object-location structure:
;; (<object> <location>)
(defparameter *command-help-text*
  '((help (display this help text.))
    (walk (walk in a direction.))
    (look (Take a look around.))
    (pickup (pick something up off the floor.))
    (drop (drop something from your inventory))
    (inventory (check your inventory))
    (quit (quit the game))))

(defparameter *nodes* '((living-room (You are in the living-room.
				      A wizard is snoring quite loudly on the couch.))
			(garden (you are in a beautiful garden.
				 There is a well in front of you.))
			(attic (you are in the attic.
				There is a giant welding torch in the corner.))))

(defparameter *edges* '((living-room (garden west door)
			 (attic upstairs ladder))
			(garden (living-room east door))
			(attic (living-room downstairs ladder))))

(defparameter *objects* '(whiskey bucket frog chain))

(defparameter *object-locations* '((whiskey living-room)
				   (bucket living-room)
				   (chain garden)
				   (frog garden)))

(defparameter *location* 'living-room)

(defparameter *allowed-commands* '(look walk pickup inventory drop help))

(defun objects-at (loc objs obj-locs)
  (labels ((at-loc-p (obj)
	     ;; look up the ohjects location, and compare it to the location passed in
	     (eq (cadr (assoc obj obj-locs)) loc)))
    ;; remove objects from the list if their current location is not the location passed in
    (remove-if-not #'at-loc-p objs)))

(defun describe-location (location nodes)
  (cadr (assoc location nodes)))

(defun describe-path (edge)
  `(there is a ,(caddr edge) going ,(cadr edge) from here.))


(defun describe-paths (location edges)
  (apply #'append (mapcar #'describe-path (cdr (assoc location edges)))))

(defun describe-objects (loc objs obj-locs)
  (labels ((describe-obj (obj)
	     `(you see a ,obj on the floor.)))
    (apply #'append (mapcar #'describe-obj (objects-at loc objs obj-locs)))))


;; `look` is not functional, depends on global/external state
(defun look ()
  (append (describe-location *location* *nodes*)
	  (describe-paths *location* *edges*)
	  (describe-objects *location* *objects* *object-locations*)))

;; display help for a command. I would like this to, if we don't specify and option,
;; print out every single help entry on a new line...
(defun help (command)
  (let ((info (assoc command *command-help-text*)))
    (if info
	(cdr info)
	;; it's ugly and sort of works. :(
	(append '(No such command. Valid commands "are:" ) (mapcar #'car *command-help-text*)))))


;; find works like this:
;;(find 'y '((5 x) (3 y) (7 z)) :key #'cadr)
;; => (3 Y)

;;(find 3 '((5 x) (3 y) (7 z)) :key #'car)
;;=> (3 y)

;; (find 6 '((5 x) (3 y) (7 z)) :key #'car)
;; => NIL

;; direction should be one of '(upstairs downstairs west east)
;; remember that the 'edges' looks like '(location direction via)
(defun walk (direction)
  (let ((next (find direction
		    ;; (assoc *location* *edges) => (living-room (garden west door) (attic upstairs ladder))
		    (cdr (assoc *location* *edges*))
		    :key #'cadr))) ;; so we want to look at the 2nd element of the edge
    (if next
	(progn (setf *location* (car next))
	       (look))
	'(you cannot go that way.))))

;;picking up objects

(defun pickup (object)
  (cond ((member object
		 (objects-at *location* *objects* *object-locations*))
	 (push (list object 'body) *object-locations*)
	 `(you are now carrying the ,object))
	(t '(you cannot get that.))))

(defun drop (object)
  (cond ((member object
		 (objects-at 'body *objects* *object-locations*))
	 (push (list object *location*) *object-locations*)
	 `(you have dropped the ,object in the ,*location*))
	(t '(you cannot drop that.))))

;; push adds something to the front of a list and updates the variable
;; the two formas are basically equivalent:
;; (defparameter *foo* '(1 2 3))
;; (push 7 *foo*)
;; (setf (cons 7 *foo*))

;; "the `assoc` command, which we use to find objects in a given location (within `objects-at`) always
;; returns the first item that it finds in a list. therefore, using the `push` command makes
;; the `assoc` command behave as if the value in the list for a given key has been replaced altogether."
;; huh.

;; shows us what's currently at the 'body location
(defun inventory ()
  (cons 'items- (objects-at 'body *objects* *object-locations*)))

;; what we've learned this chapter:
;; game world can be represented by a _graph_ made up of _nodes_ for the places the player can visit and
;; _edges_ for the paths between nodes

;; some other stuff I also already know

;; to replace a value from an association list (alist), you `push` new items onto a list
;; only the most recent value will be reported by the `assoc` function


;; not bad.... but not good either
;; (defun game-repl ()
;;   (loop (print (eval (read)))))

;; basically, unless game-read gives us 'quit, print out the evaluated game cmd and then continue repl
(defun game-repl ()
  (let ((cmd (game-read)))
    (unless (eq (car cmd) 'quit)
      (game-print (game-eval cmd))
      (game-repl))))


;; given a input containing many words separated by spaces, e.g. "one two three"
;; produces a list of the form  (one 'two 'three)
(defun game-read ()
  (let ((cmd (read-from-string
	      (concatenate 'string "(" (read-line) ")"))))
    (flet ((quote-it (x)
	     (list 'quote x)))
      (cons (car cmd) (mapcar #'quote-it (cdr cmd))))))

;; validates input from game-read to make sure it is one of the allowed commands
;; if so, it evaluates. Otherwise it notifies the player.
(defun game-eval (sexp)
  (if (member (car sexp) *allowed-commands*)
      (eval sexp)
      '(i do not know that command)))


;; does a lot of heavy lifting
;; i'd rather not document it any more than i have
(defun tweak-text (lst caps lit)
  (when lst
    (let ((item (car lst))
	  (rest (cdr lst)))
      (cond ((eql item #\space) (cons item (tweak-text rest caps lit)))        ;; space, cons it on
	    ((member item '(#\! #\? #\.)) (cons item (tweak-text rest t lit))) ;; punctuation, capitalize next
	    ((eql item #\") (tweak-text rest caps (not lit)))                  ;; quote, toggle literal (dont cons)
	    (lit (cons item (tweak-text rest nil lit)))                        ;; literal, cons next w/o changing
	    (caps (cons (char-upcase item) (tweak-text rest nil lit)))         ;; caps, capitalize just this item
	    (t (cons (char-downcase item) (tweak-text rest nil nil)))))))      ;; else, cons downcase on

;; basically all the heavy lifting here is done by tweak-text
;; turn whatever list we get into a string  (print1-to-string)
;; trim spaces and '()' off the ends of the string (string-trim)
;; turn the string into a list of characters (coerce 'list)
;; run it through tweak-text (starting with caps set to true and literal set to false)
;; turn the list of chars back into a string (coerce 'string)
;; princ out the string
(defun game-print (lst)
  (princ (coerce (tweak-text (coerce (string-trim "() "
						  (prin1-to-string lst))
				     'list)
			     t
			     nil)
		 'string))
  (fresh-line))
