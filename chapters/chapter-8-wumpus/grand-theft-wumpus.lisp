(load "../chapter-7-beyond-basic-list/graph-util")


(defparameter *congestion-city-nodes* nil)
(defparameter *congestion-city-edges* nil)
(defparameter *visited-nodes* nil)
(defparameter *player-pos* nil)
(defparameter *node-num* 30)
(defparameter *edge-num* 45)
(defparameter *worm-num* 3)
(defparameter *cop-odds* 15) ;; 1 in 15 chance


(defun random-node ()
  (1+ (random *node-num*))) ;; `random` selects a random integer in the range (0, <arg>)

;; e.g.
;; (edge-pair 10 11) ==> ((10 . 11) (11 . 10))
;; (edge-pair 1 1) ==> nil
(defun edge-pair (a b)
  (unless (eql a b)
    (list (cons a b) (cons b a))))

;; runs (edge-pair (random-node) (random-node)) *edge-num* times
(defun make-edge-list ()
  (apply #'append (loop repeat *edge-num*
			collect (edge-pair (random-node) (random-node)))))


;; loop examples:
;; creating a list of numbers:
;;(loop repeat 10
;;    collect 1)
;;  => (1 1 1 1 1 1 1 1 1 1)

;; we can also create ranges
;;(loop for n from 1 to 10
;;      collect n)
;; => (1 2 3 4 5 6 7 8 9 10)

;;(loop for n from 1 to 10
;;      collect (+ 100 n)) ;; any lisp code can go in the `collect`
;; => (101 102 103 104 105 106 107 108 109 110)


;; we have to connect the islands in our edge list now...

;; this gets a list of all edges that start from a given node.
;; remove-if-not is basically a filter function
(defun direct-edges (node edge-list)
  (remove-if-not (lambda (x)
		   (eql (car x) node))
		 edge-list))


;; so this is basically depth-first search
(defun get-connected (node edge-list)
  (let ((visited nil))
    (labels ((traverse (node)
	       (unless (member node visited) ;;unless the node is a member of the visited list, do the following:
		 (push node visited) ;; push the node onto the visited list
		 (mapc (lambda (edge) ;;and then, on each edge starting with each connected node
			 (traverse (cdr edge))) ;; traverse the destination nodes of the edge
		       (direct-edges node edge-list))))) ;; this gets the list of edges starting with current node
      (traverse node)) ;; start traversal with the current node
    visited)) ;; return the visited list, which we have jam-packed with every connected node

(defun find-islands (nodes edge-list)
  (let ((islands nil)) ;; we will be returning this
    (labels ((find-island (nodes)
	       ;; let* does the bindings sequentially, which allows us to build up 
	       (let* ((connected (get-connected (car nodes) edge-list)) ;; nodes connected to (car nodes)
		      (unconnected (set-difference nodes connected))) ;; set-difference of connected nodes and the rest of the nodes
		 (push connected islands) ;; push the group of connected nodes to the islands list
		 (when unconnected        ;; if there is anything left in the unconnected list, find islands
		   ;; because `unconnected` is the set difference of the connected nodes and the rest of the nodes
		   ;; we can be certain we won't try and find islands starting with a node that's already traversed
		   (find-island unconnected))))) 
      (find-island nodes)) ;; start by finding islands with the set of all nodes
    islands)) ;; return the list of islands we have built up


;; generates a set of edges which connect the islands.
(defun connect-with-bridges (islands)
  (when (cdr islands)
    ;; as long as there is more than 1 island left, make a bridge between the first island and the next island
    (append (edge-pair (caar islands)  (caadr islands)) ;;(caar l) = (car (car l)), (caadr l) = (car (car (cdr l)))
	    (connect-with-bridges (cdr islands)))))

;; finds islands, makes bridges between then, and adds them to the set of edges
(defun connect-all-islands (nodes edge-list)
  (append (connect-with-bridges (find-islands nodes edge-list)) edge-list))


;; to complete our edges for congestion city, we need to convert the edges from an edge list into an alist.
;; we will also add the police roadblocks, which will appear randomly on some of the edges.
;; For these tasks, we will create the make-city-edges, edges-to-alist, and add-cops functions

(defun make-city-edges ()
  (let* ((nodes (loop for i from 1 to *node-num* ;; make list of nodes
		   collect i))
	 ;; make list of edges (including bridges between islands)
	 (edge-list (connect-all-islands nodes (make-edge-list))) 
	 ;; randomly generate a list of edges that have cop roadblocks on them, using `remove-if-not`
	 (cops (remove-if-not (lambda (x) 
				;; get a random number in the range [0, *cop-odds*], if it's random then T
				(zerop (random *cop-odds*))) 
			      edge-list)))
    (add-cops (edges-to-alist edge-list) cops)))

(defun edges-to-alist (edge-list)
  (mapcar (lambda (src-node) ;; for each node in the list of nodes...
	    (cons src-node  ;; cons src-node onto the list of destinations, as the alist key
		  (mapcar (lambda (edge)       ;; basically, this gets destinations starting with src-node
			    (list (cdr edge))) ;; and makes each one into a singleton list
			  (remove-duplicates (direct-edges src-node edge-list) ;; edges starting from src-node
					     :test #'equal))))
	  (remove-duplicates (mapcar #'car edge-list)))) ;; gets list of nodes
    

(defun add-cops (edges-alist edges-with-cops) ;; edges-with-cops is a regular list, not an alist
  (mapcar (lambda (x)
	    (let ((node1 (car x)) ;; src node
		  (node1-edges (cdr x))) ;; destinations from src-node
	      (cons node1                ;; cons node1 back on after doing some transformation to tail
		    (mapcar (lambda (edge) 
			      (let ((node2 (car edge))) ;; destination edge
				(if (intersection (edge-pair node1 node2) ;; hmm...
						  edges-with-cops
						  :test #'equal)
				    (list node2 'cops) ;; if (node1 . node2) is in edges-with-cops, append 'COPS
				    edge))) ;; otherwise just give the edge back without changing it
			    node1-edges))))
	  edges-alist))
			      

;; consider a city with 3 locations:
;; 1 === 2 === 3
;; we would describe this with the edge-list '((1 . 2) (2 . 1) (2 . 3) (3 . 2))

;; or, we could describe it with the alist '((1 (2)) (2 (1) (3)) (3 (2)))

;; what if we add a roadblock, like so?
;; 1 === 2 =!= 3
;; then we could describe it with the edge-alist: '((1 (2)) (2 (1) (3 COPS)) (3 (2 COPS)))
;; here we are using a nested alist.
;;
;; with the edges in this format, we can easily find all edges connected to a given node by calling
;; `(cdr (assoc node1 edges))
;; and to see if a given edge has cops, we can call
;; (cdr (assoc node2 (cdr (assoc node1 edges))))
;; these two cdrs reach down two levels to get the data associated with the edge between two nodes.

(defun neighbors (node edge-alist)
  (mapcar #'car (cdr (assoc node edge-alist))))

;;(defun neighbors-to-depth (node edge-alist depth)
;;  (flet* ((neighboring (current-node) (neighbors (current-node edge-alist)))
;;	  (neighbors-recur (current-node depth)
;;			   (if (eq 1 depth) (neighboring current-node)
;;			       (append (
	 
  

(defun within-one (a b edge-alist)
  (member b (neighbors a edge-alist)))

(defun within-two (a b edge-alist)
  (or (within-one a b edge-alist) ;; check if b is within-one of a
      (some (lambda (x)  
	      (within-one x b edge-alist))  ;; check if b is within-one of any of a's neighbors
	    (neighbors a edge-alist)))) ;; get the list of neighbors of a

(defun has-cops (edge)
  (member 'cops edge))
  ;;(cdr edge))

(defun make-city-nodes (edges-alist)
  (let ((wumpus (random-node))
	(glow-worms (loop for i below *worm-num*
			  collect (random-node))))
    (loop for n from 1 to *node-num*
	  ;; by using append, we can add zero, one, or multiple items to the node description
	  collect (append (list n) ;; if any condition holds, make a list of node features with curnode at the head
			  (cond ((eql n wumpus) '(wumpus)) ;; checks if current node is within two of the wumpus
				((within-two n wumpus edges-alist) '(blood!)))
			  (cond ((member n glow-worms)
				 '(glow-worm))
				((some (lambda (worm)
					 (within-one n worm edges-alist)) ;; checks if current node is adjacent
				       glow-worms)                       ;; to any one of the glow-worms
				 '(lights!)))
			  (when (some #'has-cops (cdr (assoc n edges-alist)))
			    '(sirens!))))))
					   
(defun new-game ()
  (setf *congestion-city-edges* (make-city-edges))
  (setf *congestion-city-nodes* (make-city-nodes *congestion-city-edges*))
  (setf *player-pos* (find-empty-node *congestion-city-nodes*))
  (setf *visited-nodes* (list *player-pos*))
  (draw-city)
  (draw-known-city))

(defun find-empty-node (nodes)
  (let ((x (random-node)))
    (if (cdr (assoc x nodes))
	(find-empty-node nodes)
	x)))
		
(defun draw-city ()
  (ugraph->png "city" *congestion-city-nodes* *congestion-city-edges*))

(defun known-city-nodes ()
  (mapcar (lambda (node)
	    (if (member node *visited-nodes*)
		(let ((n (assoc node *congestion-city-nodes*))) ;; e.g. (5 BLOOD!)
		  (if (eql node *player-pos*) ;; depending on if this node is the player node or not
		      (append n '(*))  ;; append a '* to mark our position
		      n)) ;; or just get the node description without updating
		(list node '?))) ;; this node isn't visited yet, so we don't know anything about it.
	  (remove-duplicates
	   (append *visited-nodes* ;; append visited nodes onto list of adjacent nodes.
		   ;; mapcan - concatenated results. flatmap. 
		   (mapcan (lambda (node)  ;; this is basically getting all adjacent nodes to visited nodes
			     (mapcar #'car
				     (cdr (assoc node
						 *congestion-city-edges*))))
			   *visited-nodes*)))))


(defun known-city-edges ()
  (mapcar (lambda (node)  
	    ;; we are creating an alist here, containing only the edges we know.
	    ;; so, we are consing the node on to the (modified) cdr of the node's edge-destinations
	    (cons node (mapcar (lambda (x) ;; x is an edge destination
				 (if (member (car x) *visited-nodes*) ;; if we've been to the edge's destination...
				     x ;; we've traversed this edge, so we know everything about it.
				     (list (car x)))) ;; remove cops, since we haven't traversed this edge yet.
			       ;; every time we are doing '(assoc node edge-alist)
			       ;;   we are fetching the edges of a node
			       ;; when we do (cdr (assoc node edge-alist))
			       ;;   we are fetching just the destinations of the edges of a node
			       (cdr (assoc node *congestion-city-edges*)))))
	  *visited-nodes*))

(defun draw-known-city ()
  (ugraph->png "known-city" (known-city-nodes) (known-city-edges)))


(defun walk (pos)
  (handle-direction pos nil))

(defun charge (pos)
  (handle-direction pos t))

(defun handle-direction (pos charging)
  (let ((edge (assoc pos ;; check if `pos` is in the list of destinations available from pos
		     (cdr (assoc *player-pos* *congestion-city-edges*))))) ;; get destinations available from pos
    (if edge
	(handle-new-place edge pos charging) ;; destination, current position, charging or not
	(princ "that location does not exist!"))))

(defun handle-new-place (edge pos charging)
  (let* ((node (assoc pos *congestion-city-nodes*)) ;; get node at pos
	 (has-worm (and (member 'glow-worm node) ;; has a glow worm in the node
			(not (member pos *visited-nodes*))))) ;; ignore the glows if we've already visited.
    (pushnew pos *visited-nodes*)
    (setf *player-pos* pos)
    (draw-known-city)
    (cond ((member 'cops edge) (princ "Uh Oh!!!! You ran into the cops, Game Over!!!"))
	  ((member 'wumpus node) (if charging
				     (princ "You shot wumpus! He's Dead! You won!")
				     (princ "You ran into the wumpus, who fucking killed you. Game over.")))
	  (charging (princ "You wasted your last damn bullet. Game over."))
	  (has-worm (let ((new-pos (random-node)))
		      (princ "You ran into the Glow Worm Gang! You're now at ")
			     (princ new-pos)
			     (handle-new-place nil new-pos nil))))))
	 
