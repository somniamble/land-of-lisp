(defpackage :graph-util
  (:use :common-lisp)
  (:export 
   :graph->png
   :ugraph->png))

(defparameter *max-label-length* 30)
(defparameter *overflow-string* "...")
;; normalized the prin1 version of an expression by
;; turning non-alphanumeric characters into underscores
;; a node in DOT format can only contains letters, digits, and underscores
(defun dot-name (exp)
  (substitute-if #\_ (complement #'alphanumericp) (prin1-to-string exp)))

;;(dot-name "this,is.a!test") ;; => "_this_is_a_test_"

(defun dot-label (exp)
  (if exp
      (let ((s (write-to-string exp :pretty nil))) ;; this unsets :pretty, so the string is left alone
	(if (> (length s) *max-label-length*)
	    (concatenate 'string (subseq s 0 (- *max-label-length* 3)) *overflow-string*)
	    s)) ;; just return the string if it's under 30 characters
      "")) ;; just return an empty string for nil

(defun node-name (node)
  (car node))

(defun node-desc (node)
  (cadr node))

(defun edge-src (edge)
  (car edge))

(defun edge-dests (edge)
  (cdr edge))

(defun edge-dest-node (edge-dest)
  (car edge-dest))

(defun edge-dest-data (edge-dest)
  (cdr edge-dest))


;;(dot-label '(this is a long string is it long enough?))

;; mapc performs the action on the list but doesn't return it
;; so we use `mapc` when we are interested in the _side effects_ of the operation on the list
;; and `mapcar` when we are interested in the primary effects of the operation on the list

;; it's a common idiom to write out data to the console as an intermediary
;; and then write something which ingests from the console and writes to file
;; this lets us inspect the data. think of transparent pipes...

;; we want to format the nodes like this:
;; <node name>[label="<text of label>"];
(defun nodes->dot (nodes)
  (mapc (lambda (node)
	  (fresh-line)
	  (princ (dot-name (node-name node)))
	  (princ "[label=\"")
	  ;;(princ (dot-label (node-desc node)))
	  (princ (dot-label node))
	  (princ "\"];"))
	nodes))

;; we want to format the edges like this
;; <src node name>-><dest node name>[label="<text of label>"];
;; remember that our edges are formatted like:
;; (<src> (<dest> <direction> <via>) ...)
;; e.g. (garden (living-room east door))
;; (living-room (garden west door) (attic upstairs label))
(defun edges->dot (edges)
  (mapc (lambda (edge)
	  (mapc (lambda (destination)
		   (fresh-line)
		   (princ (dot-name (edge-src edge)))
		   (princ "->")
		   (princ (dot-name (edge-dest-node destination)))
		   (princ "[label=\"")
		   (princ (dot-label (edge-dest-data destination)))
		   (princ "\"];"));)
		(edge-dests edge)))
	edges))

;;(defun edges->dot (edges)
;;  (mapc (lambda (node-edge)
;;	  (mapc (lambda (edge)
;;		 (let ((src-node (car node-edge))       ;; garden
;;		       (dest-node (car edge))           ;; living-room
;;		       (direction-and-via (cdr edge)))    ;; (east door)
;;		   (fresh-line)
;;		   (princ (dot-name src-node))
;;		   (princ "->")
;;		   (princ (dot-name dest-node))
;;		   (princ "[label=\"")
;;		   (princ (dot-label direction-and-via))
;;		   (princ "\"];")))
;;		(cdr node-edge)))
;;	edges))
	  
	  
(defun graph->dot (nodes edges)
  (princ "digraph{")
  (nodes->dot nodes)
  (edges->dot edges)
  (princ "}"))

;; why does this work? well... recall that `let` creates _lexical_, or local variables
;; also recall that `with-open-file` creates a _lexical_ variable, analogously to `let`
;; hence, it usually creates a `lexical` stream variable for us
;; HOWEVER
;; if the dynamic variable already exists with the sane name, `let` will temporarily override
;;   the value of the dynamic variable to the new value
;; `*standard-output*` is just such a dynamic variable, and we can override its value.
;; hence, any `princ` statements in the body of the `with-open-file` will write to *standard-output*
;; but *standard-output* is the stream that we created!!! muahaha~!
;; *ahem*
;; in this way, we can re-route thunked princ statements to a file instead of the console
;; wowee does it feel like a hack :)
(defun dot->png (fname thunk)
  (with-open-file (*standard-output*      ;; specify stdout as the stream we are using here
		   fname                  ;; write to fname
		   :direction :output     ;; specify the direction we write, output
		   :if-exists :supersede) ;; specify that if fname already exists, fucking clobber it
    (funcall thunk)) ;; more on _thunk_ below ;; (funcall thunk) writes to stdout
  ;;we are using sbcl, and the technique for executing a shell command is different
  (sb-ext:run-program "dot" `("-Tpng" "-O" ,fname) :search T))
  ;;(ext:shell (concatenate 'string "dot -Tpng -O " fname))) ;; and I guess this is executing the `dot` shell command

;;tying it all together:
(defun graph->png (fname nodes edges)
  (dot->png fname
	    (lambda () ;; create our thunk here
	      (graph->dot nodes edges))))

;; using thunks:
;; it's common to create small functions which take 0 arguments, i.e. _nullary functions_
;; these can be used to wrap up a bit of computation which we want to delay until later
;; this bit of wrapped up computation is referred to as a _thunk_ or a _suspension_
;; (in the case above, we are going to wrap up (graph->dot *nodes* *edges*) into a thunk)

;; why are we thunking `graph->dot`? Well:
;; `graph->dot` writes output to the console, stdout. This makes it easier to debug
;; however it also means we need to hook stdout up to something else if we wish to capture it
;; so we can pass it to `dot->png` as a thunk, now `dot->png` is responsible for
;; calling it `(funcall thunk)`, capturing its output and writing it`(with-open-file (*standard-output* ...))`

;; it's very common to generate textual data with a computer program, so this idiom/pattern/technique is common:
;; 1. Print stuff to the console (stdout) with a function
;; 2. wrap the function in a _thunk_
;; 3. redirect `(funcall thunk)` to some other location

;; of course, this requires side effects, and lispers who write functional code eschew such things...it 

;; "to get a feel of how `with-open-file` works, here's an example that creates
;; a new file named _testfile.txt_ and writes the text "Hello File~" to it

;; (with-open-file (my-stream              ;; use the variable name `my-stream`
;; 		 "testfile.txt"         ;; use the filename "testfile.txt"
;; 		 :direction :output     ;; specify we are writing out to this file using this stream
;; 		 :if-exists :supersede) ;; specify we are re-writing the file using this stream
;;   (princ "Hello File!" my-stream))      ;; write "Hello File!" to the stream named `my-stream` we just created

;; 
;; (with-open-file (my-stream ...)
;;   ...body has my-stream defined...)
;; 
;; (let ((my-variable ...))
;;   ... body has my-variable defined ...)


;; about keyword arguments: we return again to out earlier example:

;; (with-open-file (my-stream           
;; 		 "testfile.txt"         
;; 		 :direction :output     ;; the :direction key, with value :output
;; 		 :if-exists :supersede) ;; the :if-exists key, with value :supersede
;;   (princ "Hello File!" my-stream))   
;;
;; sometimes we want a symbol to refer to something else, e.g.
;; (let ((five 5)) five) ; ==> 5
;;
;; other times we want a symbol to refer exactly to itself
;; :cigar ;; ==> :CIGAR
;; (let ((:cigar 5))
;;   :cigar)  ;; *** - LET: :CIGAR is a constant, and may not be used as a variable
;; 

;; a colon-prepended symbol in Common Lisp is referred to as a ***KEYWORD SYMBOL***
;; and a ***KEYWORD SYMBOL*** always means intself.

;;

;; <src node name>--<dest node name>[label="<text of label>"];
;; remember that our edges are formatted like:
;; (<src> (<dest> <direction> <via>) ...)
;; e.g. (garden (living-room east door))
;; (living-room (garden west door) (attic upstairs label))
;; at each step, maplist is working with the successive cdring of the list
(defun uedges->dot (edges)
  (maplist (lambda (lst)
	     (mapc (lambda (destination)
		      ;; compares the destination of the edge
		      ;; with the source-nodes of the rest of the edges
		     (unless (assoc (edge-dest-node destination) (cdr lst)) 
		       (fresh-line)
		       (princ (dot-name (caar lst)))
		       (princ "--")
		       (princ (dot-name (edge-dest-node destination)))
		       (princ "[label=\"")
		       (princ (dot-label (edge-dest-data destination)))
		       (princ "\"];")))
		   (cdar lst))) ;; (cdr (car lst))
	   ;; pops the front of the list off and gets the destinations
	   edges))

(defun ugraph->dot (nodes edges)
  (princ "graph{")
  (nodes->dot nodes)
  (uedges->dot edges)
  (princ "}"))

;; makes an undirected graph
(defun ugraph->png (fname nodes edges)
  (dot->png fname
	    (lambda ()
	      (ugraph->dot nodes edges))))
