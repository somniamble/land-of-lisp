;; Play the classic game of Robots! All robots move toward the player.
;; Robot collisions cause scrap that is deadly to other robots.
;; Teleport as a last resort!

;; for x = y ... then `operate on x`
;; this basically says: for the first iteration, use the value `y` for `x`
;; then, for successive iterations, apply the operation to x.

(defun robots ()
  (loop named main ;; by naming it main, we can use "return from" to exit early
     with directions = '((q . -65) (w . -64) (e . -63) (a . -1) ;; the 8 offsets when the game board is 64 wide
			 (d .   1) (z .  63) (x .  64) (c . 65))
     for pos = 544
     then (progn (format t "~%qwe/asd/zxc to move, (t)eleport, (l)eave:")
		 (force-output)
		 (let* ((c (read)) ;; read a char
			(d (assoc c directions))) ;; retrieve the direction from our a-list of directions
		   (cond (d (+ pos (cdr d))) ;; when d not nil, add (cdr d) to pos
			 ((eq 't c) (random 1024)) ;; randomize our position
			 ((eq 'l c) (return-from main 'bye)) ;; player leaves the game ;)
			 (t pos))))
     for monsters = (loop repeat 10
		       collect (random 1024))
     then (loop for mpos in monsters 
		collect (if (> (count mpos monsters) 1)
			    mpos ;; if there's more than one monster at mpos, just give back mpos
			    (cdar (sort (loop for (k . d) in directions
					      for new-mpos = (+ mpos d)
					      collect (cons (+ (abs (- (mod new-mpos 64)
								       (mod pos 64)))
								    (abs (- (ash new-mpos -6)
									    (ash pos -6))))
							    new-mpos))
					'<
					:key #'car))))
	when (loop for mpos in monsters
		   always (> (count mpos monsters) 1))
	return 'player-wins
	do (format t
		   "~|~{~<|~%|~,65:;~A~>~}|"
		   (loop for p
			   below 1024
			 collect (cond ((member p monsters) ;; if the position is a monster pos
					(cond ((= p pos) (return-from main 'player-loses))
					      ((> (count p monsters) 1) #\#) ;; draw scrap for doubled up robots
					      (t #\A)))
				       ((= p pos)
					#\@)
				       (t #\ ))))))
