

(format t "Add onion rings for only ~$ more!" 1.5)
;; ==> Add onion rings for only 1.50 more!

;; anatomy of this sexp...
(format ;; function name
	t ;; The destination parameter -- `t` = print to console
	  ;; `nil` -- create a string
	  ;; `<stream>` -- output to stream
	"Add onion rings for only ~$ more!" ;; the control string, with a control sequence `~$`
	   ;; every control sequence starts with a `~`
	1.5 ;; a value parameter (may be any number of these)
	)


(princ (reverse
	(format nil "this ~a test ~%" "is a")))
 ;;"
 ;;tset a si siht"       

; going to be using ~% to do newlines and stuff too

;; remember print, prin1 and princ?
;; print writes a string to a stream representing the object in lisp semantics (print/read symmetry)
;; prin1 is like print but with no newline
;; princ writes a string to a stream representing the object in human terms

;; we can use the ~s and ~a control sequences with `format` to
;;   produce the same behavior as prin1 and princ, respectively.
;; ~s = prin1
;; ~a = princ

(format t "I am printing ~s in the middle of this sentence." "foo")
;; I am printing "foo" in the middle of this sentence.NIL

(format t "I am princing ~a in the middle of this sentence." "foo")
;; I am princing foo in the middle of this sentence.

;; THE FIRST CONTROL SEQUENCE PARAMETER 
;; control sequences can be parameterized as well
;; right fixed-width pad
(format t "I am printing ~10a within ten spaces of room." "foo")
;; I am printing foo        within ten spaces of room.

;; left fixed-width pad, the left-pad is designated by @
(format t "I am printing ~10@a within 10 spaces of room." "foo")
;; I am printing        foo within 10 spaces of room.NIL


;; THE SECOND CONTROL SEQUENCE PARAMETER
;; the `3` here specifies that we should add spaces 3 at a time until we hit our goal of 10
;; in this case, it will overshoot 10 by 2....
;; the 3 is the second argument being passed to the ~a control sequence
(format t "I am printing ~10,3a within ten (or more) spaces of room." "foo")
;; I am printing foo          within ten (or more) spaces of room.NIL

;;(format t "I am printing ~10,3@a within ten (or more) spaces of room." "foo")


;; THE THIRD CONTROL SEQUENCE PARAMETER

;; we can use the third parameter to explicitly state a certain number
;; of characters to pad with, regardless of the minimum padding value
(format t "I am printing exactly four spaces (~,,4a) in the middle of this sentence" "foo")
;; I am printing exactly four spaces (foo    ) in the middle of this sentence

(format t "I'm printing exactly four spaces (~,,4@a) in the middle of this sentence" "foo")
;;I'm printing exactly four spaces (    foo) in the middle of this sentence

(format t "I'm printing at least 10 spaces and forcing 4 (~10,,4@a) in the middle of this sentence" "longer test value")

;; I'm printing at least 4 spaces and up to 10 spaces (    longer test value) in the middle of this sentence

;; THE FOURTH CONTROL SEQUENCE ARGUMENT
(format nil "The word (~,,4,'!a) feels very important." "foo")
;; ==> The word (foo!!!!) feels very important

;; this fails. Why? Because we have to specify the literal symbol to be used, by quoting. 
;;(format nil "The word (~,,4,!a) feels very important." "foo")

(format t "The word (~,,4,'?@a) feels very strange."  "fioo")
;; ==> The word (????fioo) feels very strange.

(format t "The word (~,,4,'?:@<~a~>) feels too strong." "foo")
;; ==> The word (????foo????) feels too strong.
;; I really don't understand what's going on here...

(format t "The word (~,,4:@<~a~>) feels too strong." "foo")
;; ==> The word (    foo    ) feels too strong.



;; FORMATTING INTEGERS

(format t "The number 1000 in hexadecimal is ~x" 1000)
;; The number 1000 in hexadecimal is 3E8

(format t "The number 1000 in binary is ~b" 1000)
;; The number 1000 in binary is 1111101000

(format t "The number 1000 in octal is ~o" 1000)
;; The number 1000 in octal is 1750 

(format t "The number 1000 in decimal is ~d" 1000)
;; The number 1000 in decimal is 1000

;; INTEGER FLAGS:
;; colon, `:`, standard digit group separator
(format t "Numbers with commas in them are ~:d times better." 1000000)
;; Numbers with commas in them are 1,000,000 times better.

;; we can set the width, as before...
(format t "I'm printing (~10d) with width 10." 999)
;; I'm printing (       999) with width 10.

;; we can also specify the character to be used for padding via the second param, e.g.
(format t "I'm printing (~10,'xd) with width 10." 999)
;; I'm printing (xxxxxxx999) with width 10.

;; FORMATTING FLOATS

;; we can round a float to a certain number of characters overall
;; pi rounded to 4 characters...
(format t "PI can be estimated as ~4f" 3.141593)
;; PI can be estimated as 3.14

;; pi rounded to 5 characters, note that these include the decimal point
;; also note that it rounds up here!
(format t "PI can be estimated as ~5f" 3.141593)
;; PI can be estimated as 3.142

;; the second argument can be used to specify the number of digits to display after `.`
(format t "PI can be estimated as ~,4f" pi)
;; PI can be estimated as 3.1416

;; the third parameter can be used to scale the numbers by factors of ten.
;; e.g. here we scale a decimal number by 10^2, making it a percentage
(format t "Percentages are ~,,2f% better than fractions" .77)
;; Percentages are 77.0% better than fractions

;; does it work the other way?
(format t "5 percent can be expressed as ~,,-2f" 5)
;; 5 percent can be expressed as 0.05
;; YES!

;; in addition to ~f, we can use ~$ to format currencies
(format t "Hey can you lend me uhhhhh ~$ dollars?" 35000.2)
;; Hey can you lend me uhhhhh 35000.20 dollars?

(format t "Hey can you lend me uhhhhh ~$ dollars?" 10.2324534)
;; Hey can you lend me uhhhhh 10.23 dollars?

;; OTHER FORMATTING TRICK

;; MULTILINE OUTPUT

;; ~% causes a newline to be created in all cases
;; ~& causes a newline to be printed only as needed, a la fresh-line

(progn (format t "this is on one line ~%")
       (format t "~%this is on another line"))
;; ==> this is on one line 
;;
;;this is on another line 

(progn (format t "This is on one line ~&")
       (format t "~&and this is on the next line"))
;;==> This is on one line 
;;and this is on the next line

;; we can parameterize these:
(progn (format t "This will print ~5% on two lines 5 lines apart"))
;;==> This will print 
;;
;;
;;
;;
;;on two lines 5 lines apart

;; TEXT JUSTIFICATION

(defun random-lemur ()
  (nth (random 5) '("Uttunul" "Murmur" "Oddubb" "Djynxx" "Katak")))

;; we can use the ~t control sequence to lay out the lemurs in a neatly formatted table
;; e.g. we cause the lemurian names to print out at `n` columns of text

(loop repeat 10
      do (format t "~5t~a ~15t~a ~25t~a~%"
		 (random-lemur)
		 (random-lemur)
		 (random-lemur)))
;;     Murmur    Katak     Murmur
;;     Oddubb    Djynxx    Djynxx
;;     Katak     Oddubb    Uttunul
;;     Katak     Murmur    Oddubb
;;     Murmur    Murmur    Djynxx
;;     Oddubb    Oddubb    Uttunul
;;     Murmur    Katak     Djynxx
;;     Katak     Murmur    Oddubb
;;     Murmur    Djynxx    Uttunul
;;     Katak     Oddubb    Uttunul


;; we can also space out our lemur friends on a single line
;; ~30< -- start justifying, and the whole line will be 30 columns wide
;; ~a -- put a value here
;; ~; -- start a new justified cell. 
;;    the ~; indicates where extra spaces should be inserted to do the text justification
;; ~a -- put a value here
;; ~; -- start a new justified cell
;; ~a -- put a value here
;; ~> -- stop justifying
;; ~% -- clap down a newline
(loop repeat 10
      do (format t "~30<~a~;~a~;~a~>~%"
		 (random-lemur)
		 (random-lemur)
		 (random-lemur)))

;;Uttunul     Uttunul     Murmur
;;Uttunul     Djynxx      Djynxx
;;Oddubb      Murmur      Oddubb
;;Katak      Murmur      Uttunul
;;Katak      Djynxx       Djynxx
;;Djynxx     Oddubb      Uttunul
;;Uttunul     Djynxx      Oddubb
;;Oddubb      Djynxx       Katak
;;Uttunul      Katak      Murmur
;;Oddubb      Murmur      Djynxx

;; we can use the :@ directive to justify each of the cells in the line...
;; but because we're doing this cell-centering per line, it comes out wavy.
(loop repeat 10
      do (format t "~30:@<~a~;~a~;~a~>~%"
		 (random-lemur)
		 (random-lemur)
		 (random-lemur)))
;;   Djynxx   Djynxx   Djynxx   
;;   Murmur   Katak    Katak    
;;   Katak   Djynxx   Uttunul   
;;   Djynxx   Katak   Uttunul   
;;  Oddubb  Uttunul   Uttunul   
;;  Uttunul   Murmur   Oddubb   
;;  Oddubb   Murmur   Uttunul   
;;   Djynxx   Djynxx   Murmur   
;;   Katak   Katak    Murmur    
;;  Djynxx   Murmur   Uttunul   

;; so, what if we specify that each column of the table should be
;; 10 spaces/columns wide
;; centered relative to those 10 spaces?

(loop repeat 10
      do (format t "~10:@<~a~>~10:@<~a~>~10:@<~a~>~%"
		 (random-lemur)
		 (random-lemur)
		 (random-lemur)))

;; success!
;;  Katak    Uttunul    Murmur  
;;  Katak     Murmur    Djynxx  
;; Uttunul    Katak     Murmur  
;;  Murmur    Murmur    Djynxx  
;;  Katak    Uttunul    Katak   
;;  Djynxx    Djynxx   Uttunul  
;; Uttunul    Katak     Katak   
;;  Oddubb    Oddubb    Katak   
;;  Katak     Murmur    Katak   
;; Uttunul    Murmur    Oddubb  

;; LIST LOOPING

;; things between ~{ and ~} are looped.
;; So, any formatting done will consume list input until there is nothing left
(format t "~{~a approaches! ~}" (loop repeat 10 collect (random-lemur)))
;; ==> Oddubb approaches! Oddubb approaches! Katak approaches! Djynxx approaches! Katak approaches! Murmur approaches! Katak approaches! Djynxx approaches! Oddubb approaches! Katak approaches!

(format t "~{I see ~a... or is it ~a?~%~}" (loop repeat 10 collect (random-lemur)))
;; I see Katak... or is it Uttunul?
;; I see Djynxx... or is it Katak?
;; I see Djynxx... or is it Uttunul?
;; I see Katak... or is it Murmur?
;; I see Djynxx... or is it Oddubb?

;; A CRAZY FORMATTING TRICK FOR CREATING PRETTY TABLES OF DATA
(format t "|~{~<|~%|~,33:;~2d ~>~}|" (loop for x below 100 collect x))

