;;; in order to make dice of doom suck 50% less cock we need to implement rudimentary lazy programming in it
;;; the basic idea is this:
;;; wrap the evaluation of some form up
;;; only evaluate it once, when we 'force' the evaluation of it
;;; after evaluating the lazy form, store the value and any time we force it again
;;; just reference the value that was computed
;;; it's so good it makes my mouth water

;;; here, like this:
#+nil(lazy (+ 1 2))
;;; => #<FUNCTION ...>

#+nil(force (lazy (+ 1 2)))
;;; => 3

(defmacro lazy (&body body)
  ;; define some gensyms to use...
  (let ((forced (gensym))
	(value  (gensym)))
    ;; these variables we define here will be captured by our lambda below
    `(let ((,forced nil)
	   (,value nil))
       (lambda ()
	 ;; if we haven't forced evaluation yet, evaluate this:
	 (unless ,forced
	   ;; evaluate body (splice it into a progn), store value
	   (setf ,value (progn ,@body))
	   ;; state that we have forced evaluation of this form
	   (setf ,forced t))
	 ;; reference the stored value
	 ,value))))

(defun force (lazy-value)
  (funcall lazy-value))

#+nil
(let ((l (lazy
	   (princ "adding values now")
	   (+ 6 6 6))))
  (format t "~a~%~a~%" (force l) (force l)))
;;=>adding values now18
;;18
;; fuck yea

(defmacro lazy-cons (a d)
  `(lazy (cons ,a ,d)))

(defun lazy-car (x)
  (car (force x)))

(defun lazy-cdr (x)
  (cdr (force x)))

(defun lazy-nil ()
  (lazy nil))

(defun lazy-null (x)
  (not (force x)))

(defun lazy-cadr (x)
  (lazy-car (lazy-cdr x)))

(defun lazy-caar (x)
  (lazy-car (lazy-car x)))

(defun lazy-cddr (x)
  (lazy-cdr (lazy-cdr x)))

(defun lazy-caddr (x)
  (lazy-cadr (lazy-cdr x)))

(defun lazy-cadddr (x)
  (lazy-caddr (lazy-cdr x)))

(defun lazy-caddddr (x)
  (lazy-caddr (lazy-cdr x)))

;; and so on and so on

(defun make-lazy (lst)
  (lazy (when lst
	  (cons (car lst) (make-lazy (cdr lst))))))

#+nil
(labels ((f (n)
       (lazy-cons n (f (1+ n)))))
  (defparameter *integers* (f 0)))

;;; (take (5 *integers*))
;;(defun take (n lst)

;;;(labels ((f (n)
;;;       (lazy-cons n (f (1+ n)))))
;;;  (defparameter *integers* (f 0))
;;;  (take 10 *integers*)
;;;  ;;; => (0 1 2 3 4 5 6 7 8 9)
(defun take (number lazylist)
  (labels ((f (n ll acc)
	     (cond ((zerop n) acc)
		   ((lazy-null ll) acc)
		   (t (f (1- n) (lazy-cdr ll) (cons (lazy-car ll) acc))))))
    (reverse (f number lazylist nil))))

;;  (cond ((zerop n) nil)
;;	((lazy-null ll) nil)
;;	(t (cons (lazy-car ll) (take (1- n) (lazy-cdr ll))))))
#+nil(labels ((f (n)
       (lazy-cons n (f (1+ n)))))
  (defparameter *integers* (f 0))
  (take 10 *integers*))
;; => (0 1 2 3 4 5 6 7 8 9)

(defun take-all (lazylist)
  (labels ((f (lst acc)
	     (if (lazy-null lst)
		 acc
		 (f (lazy-cdr lst) (cons (lazy-car lst) acc)))))
    (reverse (f lazylist nil))))

#+nil
(take-all (make-lazy '(a b c d e f g h i j k l m n o p q r s t u v w x y z)))
;;=>(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

;;  (if (lazy-null ll)
;;      nil
;;      (cons (lazy-car ll) (take-all (lazy-cdr ll)))))

(defun lazy-mapcar (fun lazylst)
  (lazy (unless (lazy-null lazylst)
	  (cons (funcall fun (lazy-car lazylst))
		(lazy-mapcar fun (lazy-cdr lazylst))))))

#+nil
(labels ((f (n)
       (lazy-cons n (f (1+ n)))))
  (defparameter *integers* (f 0)))
#+nil
(take 10 (lazy-mapcar (lambda (x) (* x x)) *integers*))
;;=>(0 1 4 9 16 25 36 49 64 81)

;; remember this is basically a flatmap
;; where the function we're operating on is going to make a list out of each element
;; and we need to 
(defun lazy-mapcan (fun lazylst)
  (labels ((f (current-lst)
	     (if (lazy-null current-lst)
		 (force (lazy-mapcan fun (lazy-cdr lazylst)))
		 (cons (lazy-car current-lst) (lazy (f (lazy-cdr current-lst)))))))
    (lazy (unless (lazy-null lazylst)
	    (f (funcall fun (lazy-car lazylst)))))))

#+nil
(take 10 (lazy-mapcan (lambda (x)
			(if (evenp x)
			    (make-lazy (list x (* x x)))
			    (lazy nil)))
			*integers*))
;;=>(0 0 2 4 4 16 6 36 8 64)

(defun lazy-find-if (p lst)
  (unless (lazy-null lst)
    (let ((x (lazy-car lst)))
      (if (funcall p x)
	  x
	  (lazy-find-if p (lazy-cdr lst))))))

#+nil
(lazy-find-if (lambda (n) (= 1000 n)) *integers*)
;; => 1000


(defun lazy-nth (n lst)
  (if (zerop n)
      (lazy-car lst)
      (lazy-nth (1- n) (lazy-cdr lst))))

#+nil
(lazy-nth 50 *integers*)
;;=>50

(labels ((f (n)
       (lazy-cons n (f (1+ n)))))
  (defparameter *integers* (f 0)))
