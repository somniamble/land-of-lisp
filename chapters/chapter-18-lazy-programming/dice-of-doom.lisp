(load "dice-of-doom-v1.lisp")
(Load "lazy.lisp")

(defparameter *board-size* 5)
(defparameter *ai-level* 4)
(defparameter *board-hexnum* (* *board-size* *board-size*))

(defun new-game () (game-tree (generate-board) 0 0 t))


;;; the list of passing moves has been made into a lazy list
;;; the whole damn game turns into a lazily evaluated game
;;; this one change will require us changing a multitude of other effects.
(defun add-passing-move (board player spare-dice first-move moves)
  (if first-move
      moves
      (lazy-cons (list nil
		       (game-tree (add-new-dice board player
						(1- spare-dice))
				  (mod (1+ player) *num-players*)
				  0
				  t))
		 moves)))

(defun attacking-moves (board cur-player spare-dice)
  (labels ((player (pos)
	     (car (aref board pos)))
	   (dice (pos)
	     (cadr (aref board pos))))
    (lazy-mapcan
     (lambda (src)
       (if (eq (player src) cur-player)
	   (lazy-mapcan
	    (lambda (dst)
	      (if (and (not (eq (player dst)
				cur-player))
		       (> (dice src) (dice dst)))
		  (make-lazy
		   (list (list (list src dst)
			       (game-tree (board-attack board
							cur-player
							src
							dst
							(dice src))
					  cur-player
					  (+ spare-dice (dice dst))
					  nil))))
		  (lazy-nil)))
	    (make-lazy (neighbors src)))
	   (lazy-nil)))
     (make-lazy (loop for n below *board-hexnum*
		      collect n)))))
	   
(defun handle-human (tree)
  (fresh-line)
  (princ "Choose you move:")
  (let ((moves (caddr tree))) ; list of moves is lazy
    (labels ((print-moves (moves n)
	       (unless (lazy-null moves)
		 (let* ((move (lazy-car moves))
			(action (car move)))
		   (fresh-line)
		   (format t "~a. " n)
		   (if action
		       (format t "~a -> ~a" (car action) (cadr action))
		       (princ "end turn")))
		 (print-moves (lazy-cdr moves) (1+ n)))))
      (print-moves moves 1))
    (fresh-line)
    (cadr (lazy-nth (1- (read)) moves))))

(defun play-vs-human (tree)
  (print-info tree)
  (if (not (lazy-null (caddr tree)))
      (play-vs-human (handle-human tree))
      (announce-winner (cadr tree))))


;; produces a lazy-tree trimmed to a given depth
(defun limit-tree-depth (tree depth)
  (list (car tree)
	(cadr tree)
	(if (zerop depth)
	    (lazy-nil)
	    (lazy-mapcar (lambda (move)
			   (list (car move)
				 (limit-tree-depth (cadr move) (1- depth))))
			 (caddr tree)))))

;;; this new version of handle-computer is working with a tree with a max depth of *ai-level*
;;; get-ratings isn't implemented to handle a lazy-list yet
(defun handle-computer (tree)
  (let ((ratings (get-ratings (limit-tree-depth tree *ai-level*)
			      (car tree))))
    (cadr (lazy-nth (position (apply #'max ratings) ratings)
		    (caddr tree)))))

(defun play-vs-computer (tree)
  (print-info tree)
  (cond ((lazy-null (caddr tree)) (announce-winner (cadr tree)))
	((zerop (car tree)) (play-vs-computer (handle-human tree)))
	(t (play-vs-computer (handle-computer tree)))))

(defparameter *threatened-pos-score* 1)
(defparameter *secure-pos-score* 2)
(defparameter *enemy-pos-score* -1)

(defun score-board (board player)
  (loop for hex across board
	for pos from 0
	sum (if (eq (car hex) player)
		(if (threatened pos board)
		    *threatened-pos-score*
		    *secure-pos-score*)
		*enemy-pos-score*)))

(defun threatened (pos board)
  (let* ((hex (aref board pos))
	 (player (car hex))
	 (dice (cadr hex)))
    (loop for n in (neighbors pos)
	  do (let* ((nhex (aref board n))
		    (nplayer (car nhex))
		    (ndice (cadr nhex)))
	       ;; when the neighboring hex is an enemy that has more dice than this pos
	       (when (and (not (eq player nplayer)) (> ndice dice))
		 ;; this return statement here breaks us out of the loop early
		 (return t)))))) 

(defun get-ratings (tree player)
  ;; take-all forces strict evaluation of lazy-mapcar
  (take-all (lazy-mapcar (lambda (move)
			   (rate-position (cadr move) player))
			 (caddr tree))))

(defun rate-position (tree player)
  (let ((moves (caddr tree)))
    (if (not (lazy-null moves))
	;; if there are moves, then evaluate them and select the one that scores the best for us
	(apply (if (eq (car tree) player)
		   #'max
		   #'min)
	       (get-ratings tree player))
	;; otherwise, score the board and return that
	(score-board (cadr tree) player))))

