(load "dice-of-doom.lisp")

;;;; just wanted to clean things up a bit and implement the alphabeta stuff in its own file.

(defun ab-get-ratings-max (tree player upper-limit lower-limit)
  (labels ((f (moves lower-limit)
	     (unless (lazy-null moves)
	       (let ((x (ab-rate-position (cadr (lazy-car moves))
					  player
					  upper-limit
					  lower-limit)))
		 (if (>= x upper-limit)
		     (list x)
		     (cons x (f (lazy-cdr moves) (max x lower-limit))))))))
    (f (caddr tree) lower-limit)))


(defun ab-get-ratings-min (tree player upper-limit lower-limit)
  (labels ((f (moves upper-limit)
	     (unless (lazy-null moves)
	       ;; get the score of the tree's position
	       (let ((x (ab-rate-position (cadr (lazy-car moves))
					  player
					  upper-limit
					  lower-limit)))
		 ;; if x, the score is less than the lower-limit
		 (if (<= x lower-limit)
		     ;; we, the minimizing player, will select this score because it is under the lower limit.
		     (list x)
		     ;; otherwise, we're still looking for a lower score, since we're trying to minimize it.
		     ;; note also that we update the upper-limit to the minimum of this score and the current.
		     (cons x (f (lazy-cdr moves) (min x upper-limit))))))))
    (f (caddr tree) upper-limit)))

(defun ab-rate-position (tree player upper-limit lower-limit)
  (let ((moves (caddr tree)))
    (if (not (lazy-null moves))
	;; if we're rating relative to the current player (i.e. trying to maximize score
	(if (eq (car tree) player)
	    ;; select the maximum value of the moves available to us
	    (apply #'max (ab-get-ratings-max tree
					     player
					     upper-limit
					     lower-limit))
	    ;; however, if we're rating a move for the opposing player
	    ;;   we want to select the minimally scoring move -- basically
	    ;;   we're selecting as if the opposing player was playing their best
	    ;;   and preventing us from getting a good score
	    (apply #'min (ab-get-ratings-min tree
					     player
					     upper-limit
					     lower-limit)))
	;; static board score
	(score-board (cadr tree) player))))

(defun handle-computer (tree)
  (let ((ratings (ab-get-ratings-max (limit-tree-depth tree *ai-level*)
				     (car tree)
				     most-positive-fixnum
				     most-negative-fixnum)))
    (cadr (lazy-nth (position (apply #'max ratings) ratings) (caddr tree)))))
