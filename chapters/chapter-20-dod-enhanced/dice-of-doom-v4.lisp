(load "dice-of-doom-v2.lisp")
(load "server.lisp")
(load "svg.lisp")


(defparameter *green* '(15 212 63))
(defparameter *black* '(0 0 0))
(defparameter *rude-red* '(250 0 0))

(defparameter *tile-colors*
  (list (list *bury-pink* *green*)
	(list *green* *nice-blue*)
	(list *nice-blue* *rude-red*)
	(list *rude-red* *black*)))

(defparameter *num-players* 4)
(defparameter *max-dice* 5)
(defparameter *ai-level* 2)

(defparameter *board-size* 8)
(defparameter *board-hexnum* (* *board-size* *board-size*))
(defparameter *board-width* 1600)
(defparameter *board-height* 1200)
(defparameter *board-scale* 64) ; radius of a single hexagon
(defparameter *top-offset* 3)   ; three extra hexes of height above the board, because of dice
(defparameter *dice-scale* 40)  ; radius of a single die
(defparameter *pip-scale* 0.10)  ; scale of the pip to a die. so a pip is 5% the size of a die.
(defparameter *pip-radius* (* *dice-scale* *pip-scale* 0.5)) ; get the radius of the pip

(defun response (document)
  (format nil "HTTP/1.1 200 OK
Content-Length: ~d
Content-Type: text/html

~a" (length document) document))


;;; x and y represent the center point of the die. color is the color... so
(defun draw-die-svg (x y color pip-color)
  (labels ((calc-pt (pt)
	     ;; translates the point by the die's center x y
	     (list (+ x (* *dice-scale* (car pt)))
		   (+ y (* *dice-scale* (cadr pt)))))
	   (f (poly color)
	     (polygon (mapcar #'calc-pt poly) color)))
    ; top rhombus
    (f '((0 -1) (-0.6  -0.75) (0 -0.5) (0.6 -0.75))
       (apply-brightness color 40))
    (f '((0 -0.5) (-0.6 -0.75) (-0.6 0) (0 0.25))
       color)
    (f '((0 -0.5) (0.6 -0.75) (0.6 0) (0 0.25))
       (apply-brightness color -40))
    (mapc (lambda (cx cy)
	    (circle (calc-pt (list cx cy)) *pip-radius* pip-color))
	  '(-0.05 0.125 0.3 -0.3 -0.125 0.05 0.2 0.2 0.45 0.45 -0.45 -0.2)
	  '(-0.875 -0.80 -0.725 -0.775 -0.70 -0.625
	    -0.35 -0.05 -0.45 -0.15 -0.45 -0.05))))
    ;(mapc (lambda (x y)
    ;	    (polygon (mapcar (lambda (xx yy)
    ;			       (calc-pt (list (+ x (* xx *dot-size*))
    ;					      (+ y (* yy *dot-size*)))))
    ;			     '(-1 -1 1 1)
    ;			     '(-1 1 1 -1))
    ;		     pip-color))
			     

#+nil
(redirect-stdout "bury-pink-die.svg" (svg 200 200
				       (draw-die-svg 50 100 *bury-pink* *nice-blue*)
				       (draw-die-svg 40 70 *bury-pink* *nice-blue*)
				       (draw-die-svg 45 40 *bury-pink* *nice-blue*)))


(defun draw-tile-svg (x y pos hex xx yy color pip-color chosen-tile)
  (loop for z below 2
	;; this draws the actual
	do (polygon (mapcar (lambda (pt)
			      (list (+ xx (* *board-scale* (car pt)))
				    (+ yy (* *board-scale*
					     (+ (cadr pt) (* (- 1 z) 0.1))))))
			    '((-1 -0.2) (0 -0.5) (1 -0.2)
			      (1 0.2) (0 0.5) (-1 0.2)))
		    (if (eql pos chosen-tile)
			(apply-brightness color 100)
			color)))
  (loop for z below (second hex)
	do (draw-die-svg (+ xx
			    (* *dice-scale*
			       0.3
			       (if (oddp (+ x y z))
					 -0.3
					 0.3)))
			 ;; offsets the y of the die by the number it is in the stack.
			 ;; remember that we want to draw the top die last, so it's drawn over the others.
			 (- yy (* *dice-scale* z 0.8)) color pip-color)))

#+nil
(redirect-stdout "nicetile.svg" (svg 300 300 (draw-tile-svg 0 0 0 '(0 3) 100 150 *bury-pink* *nice-blue* nil)))

(defun draw-board-svg (board chosen-tile legal-tiles)
  (loop for y below *board-size*
	do (loop for x below *board-size*
		 ;; the position of the hex within the board.
		 for pos = (+ x (* *board-size* y))
		 ;; the actual hexagon containing (<player> <num-dice>)
		 for hex = (aref board pos)
		 ;; i am pretty sure this is generating the center points of each tile 
		 for xx = (* *board-scale* (+ (* 2 x) (- *board-size* y)))
		 for yy = (* *board-scale* (+ (* y 0.7) *top-offset*))
		 ;; the math here for the brightness is darkening the tiles in the back a bit... 
		 ;; colors has: tile color in the front,
		 for colors = (mapcar (lambda (c) (apply-brightness c (* -15 (- *board-size* y))))
				      (nth (first hex) *tile-colors*))
		 do (if (or (member pos legal-tiles) (eql pos chosen-tile))
			(tag g ()
			  (tag a ("xlink:href" (make-game-link pos))
			    (draw-tile-svg x y pos hex xx yy (car colors) (cadr colors) chosen-tile)))
			(draw-tile-svg x y pos hex xx yy (car colors) (cadr colors) chosen-tile)))))


(defun make-game-link (pos)
  (format nil "/game.html?chosen=~a" pos))

;;(redirect-stdout "this-is-a-nice-board.svg"
;)
;;(svg *board-width* *board-height* (draw-board-svg (cadr (new-game)) 0 nil))

;;; the current game tree
(defparameter *cur-game-tree* nil)

;;; the currently selected tile I think
(defparameter *from-tile* nil)

(defun dod-request-handler (path header params)
  ;; horrible.
  (princ
  (response
   (with-output-to-string (*standard-output*)
	(if (equal path "game.html")
	    (html
		(head "Dice of Doom!")
		(body
		(tag center ()
		    (tag h1 () "Welcome to DICE OF DOOM")
		    (etag br ())
		    (let ((chosen (assoc 'chosen params)))
		    ;; if the game tree isn't initialized / if the player hasn't chosen a tile,
		    ;; that mean's we're starting up a new game.
		    (when (or (not *cur-game-tree*) (not chosen))
			(setf chosen nil)
			(web-initialize))
			    ;; if the game has ended
		    (cond ((lazy-null (caddr *cur-game-tree*))
			    ;; announce the winner
			    (web-announce-winner (cadr *cur-game-tree*)))
			    ;; if the player is 0, that means it's human's turn
			    ((zerop (car *cur-game-tree*))
			    (web-handle-human
			    (when chosen
				(read-from-string (cdr chosen)))))
			    (t (web-handle-computer))))
		    (etag br ())
		    (princ (draw-dod-page *cur-game-tree* *from-tile*)))))
	    (html
		(head "404")
		(body
		(tag h1 () (princ "this page doesn't exist :("))
		(tag a (href "game.html") (princ "return to safety?")))))))))

(defun web-initialize ()
  (setf *from-tile* nil)
  (setf *cur-game-tree* (new-game)))

(defun web-announce-winner (board)
  (fresh-line)
  (let ((w (winners board)))
    (if (> (length w) 1)
	(format t "The game is a tie between ~a ~{and ~a~}"
		(player-letter (car w)) (mapcar #'player-letter (cdr w)))
	(format t "The winner is ~a" (player-letter (car w)))))
  (tag a (href "game.html")
    (princ "Play again?")))

(defun web-handle-human (pos)
  (cond ((not pos) (princ "Please choose a hex to move from:"))
	((eq pos 'pass) (setf *cur-game-tree*
			      ;; update the current game tree to the passing game tree.
			      (cadr (lazy-car (caddr *cur-game-tree*))))
	 (princ "Your reinforcements have been placed.")
	 (tag a (href (make-game-link nil))
	   (princ "continue")))
	;; this is the first tile we're selecting, so *from-tile* is false. update it with our position
	((not *from-tile*) (setf *from-tile* pos)
	 (princ "Now make your attack:"))
	((eq pos *from-tile*) (setf *from-tile* nil)
	 (princ "Move cancelled."))
	;; apply the new game tree that we move to
	(t (setf *cur-game-tree*
		 (pick-chance-branch
		  ;; board
		  (cadr *cur-game-tree*)
		  ;; move
		  (lazy-find-if (lambda (move)
				  (equal (car move)
					 ;; search for the move described by (*from-tile* pos)
					 (list *from-tile* pos)))
				(caddr *cur-game-tree*))))
	   ;; update from-tile to nil, since we made our move
	   (setf *from-tile* nil)
	   (princ "You may now ")
	   (tag a (href (make-game-link 'pass))
	     (princ "pass"))
	   (princ " or make another move:"))))
	

(defun web-handle-computer ()
  (setf *cur-game-tree* (handle-computer *cur-game-tree*))
  (princ "The computer has moved")
  (tag script ()
    (princ
     "window.setTimeout('window.location=\"game.html?chosen=NIL\"',1000)")))

(defun draw-dod-page (tree selected-tile)
  (svg *board-width* *board-height*
    (draw-board-svg (cadr tree)
		    selected-tile
		     (take-all (if selected-tile
				   ;; gets the list of destinations from moves starting with selected-tile
				   (lazy-mapcar
				    (lambda (move)
				      (when (eql (caar move)
						 selected-tile)
					(cadar move)))
				    (caddr tree))
				   
				   ;; gets the starting positions from all moves currently avaiable
				   (lazy-mapcar #'caar (caddr tree)))))))



;;; new attacking rules stuff here

(defun attacking-moves (board cur-player spare-dice)
  (labels ((player (pos)
	     (car (aref board pos)))
	   (dice (pos)
	     (cadr (aref board pos))))
    ;; map over all source tiles which belong to the current player
    (lazy-mapcan (lambda (src)
		   ;; map over all neighboring enemy destination tiles
		   (lazy-mapcan (lambda (dst)
				  (make-lazy
				   (list
				    (list
				     (list src dst)
				     (game-tree (board-attack board cur-player src dst (dice src))
						cur-player
						(+ spare-dice (dice dst))
						nil)
				     (game-tree
				      (board-attack-fail board cur-player src dst (dice src))
				      cur-player
				      (+ spare-dice (dice dst))
				      nil)))))
				(lazy-remove-if-not (lambda (dst)
						      (not (eq (player dst) cur-player)))
						    (make-lazy (neighbors src)))))
		 ;; filter out all of the invalid sources
		 (lazy-remove-if-not (lambda (src)
				       ;; src belongs to current player
				       (and (eq (player src) cur-player) 
					    ;; src has more than one die
					    (> (dice src) 1)))
				     (make-lazy (loop for n below *board-hexnum*
						      collect n))))))

#+nil
(defun attacking-moves (board cur-player spare-dice)
  (labels ((player (pos)
	     (car (aref board pos)))
	   (dice (pos)
	     (cadr (aref board pos))))
    (lazy-mapcan (lambda (src)
		   (if (eq (player src) cur-player)
			   (lazy-mapcan
			    (lambda (dst)
			      (if (and (not (eq (player dst) cur-player))
				       (> (dice src) 1))
				  (make-lazy (list
					      (list
					       (list src dst)
					       (game-tree (board-attack board cur-player src dst (dice src))
							  cur-player
							  (+ spare-dice (dice dst))
							  nil)
					       (game-tree (board-attack-fail board cur-player src dst (dice src))
									     cur-player
									     (+ spare-dice (dice dst))
									     nil))))
				  (lazy-nil)))
			    (make-lazy (neighbors src)))
			   (lazy-nil)))
		 (make-lazy (loop for n below *board-hexnum*
				  collect n)))))


(defun board-attack-fail (board player src dst dice)
  (board-array (loop for pos from 0
		     for hex across board
		     collect (if (eq pos src)
			     (list player 1)
			     hex))))


(defun roll-dice (dice-num)
  (let ((total (loop repeat dice-num
	       sum (1+ (random 6)))))
    (format t "~%On ~a dice rolled ~a. ~%" dice-num total)
    total))

;;; defenders win ties
(defun roll-against (src-dice dst-dice)
  (> (roll-dice src-dice) (roll-dice dst-dice)))

(defun pick-chance-branch (board move)
  (labels ((dice (pos)
	     (cadr (aref board pos))))
  (let ((path (car move)))
    ;; if the path is nil (pass), or if the attacker wins the roll-off
    (if (or (null path) (roll-against (dice (car path))
				      (dice (cadr path))))
	;; take the game board resulting from the success
	(cadr move)
	;; otherwise take the game board representing a failure.
	(caddr move)))))
(defun handle-computer (tree)
  (let ((ratings (get-ratings (limit-tree-depth tree *ai-level*) (car tree))))
    (pick-chance-branch
     (cadr tree)
     (lazy-nth (position (apply #'max ratings) ratings) (caddr tree)))))

(defparameter *dice-probability* #(#(0.84 0.97 1.0 1.0)
				   #(0.44 0.78 0.94 0.99)
				   #(0.15 0.45 0.74 0.91)
				   #(0.04 0.19 0.46 0.72)
				   #(0.01 0.06 0.22 0.46)))

(defun get-ratings (tree player)
  (let ((board (cadr tree)))
    (labels ((dice (pos)
	       (cadr (aref board pos))))
      (take-all (lazy-mapcar
		 (lambda (move)
		   (let ((path (car move)))
		     (if path
			 (let* ((src (car path))
				(dst (cadr path))
				(probability (aref (aref *dice-probability*
							 (1- (dice dst)))
						   (- (dice src) 2))))
			   (+ (* probability (rate-position (cadr move) player))
			      (* (- 1 probability) (rate-position (caddr move) player))))
			 (rate-position (cadr move) player))))
		 (caddr tree))))))

(defun limit-tree-depth (tree depth)
  (list (car tree)
	(cadr tree)
	(if (zerop depth)
	    (lazy-nil)
	    (lazy-mapcar (lambda (move)
			   (cons (car move)
				 (mapcar (lambda (x)
					   (limit-tree-depth x (1- depth)))
					 (cdr move))))
			 (caddr tree)))))


;;; new reinforcement rules stuff

(defun get-connected (board player pos)
  ;; checks a single position and appends any new neighbors accessible from that position to the visited list
  (labels ((check-pos (pos visited)
	     ;; position belongs to player
	     (if (and (eq (car (aref board pos)) player)
		      ;; position has not been visited
		      (not (member pos visited)))
		 ;; then check the neighbors of the position, and add this position to the list of visited
		 (check-neighbors (neighbors pos) (cons pos visited))
		 ;; otherwise simply return the list of visited positions
		 visited)) 
	   ;; calls check-pos on the car of the list, and check-neighbors on every other item.
	   (check-neighbors (lst visited)
	     (if lst
		 ;; 
		 (check-neighbors (cdr lst) (check-pos (car lst) visited))
		 ;; otherwise return the list of visited positions
		 visited)))
    ;; calls check-pos on the first position.
    (check-pos pos '())))

(defun largest-cluster-size (board player)
  (labels ((f (pos visited best)
	     ;; if position is within hexnum
	     (if (< pos *board-hexnum*)
		 (if (and (eq (car (aref board pos)) player)
			  (not (member pos visited)))
		     (let* ((cluster (get-connected board player pos))
			    (size (length cluster)))
		       (if (> size best)
			   (f (1+ pos) (append cluster visited) size)
			   (f (1+ pos) (append cluster visited) best)))
		     (f (1+ pos) visited best))
		 best)))
    (f 0 '() 0)))

(defun add-new-dice (board player spare-dice)
  ;; lst is the board array coerced into list form
  (labels ((f (lst n)
	     (cond ((zerop n) lst)
		   ((null lst) nil)
		   (t (let ((cur-player (caar lst))
			    (cur-dice (cadar lst)))
			(if (and (eq cur-player player) (< cur-dice *max-dice*))
			    (cons (list cur-player (1+ cur-dice))
				  (f (cdr lst) (1- n)))
			    (cons (car lst) (f (cdr lst) n))))))))
    (board-array (f (coerce board 'list)
		    (largest-cluster-size board player)))))
