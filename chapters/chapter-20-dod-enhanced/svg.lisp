;;; given a list, groups every 2 elements together into a sublist of length 2
;;; if the list length is even, all elements are included in resulting list of pairs
;;; if the list length is odd, the last element of the list is omitted. Odd one out!! 
(defun pairs (lst)
  (labels ((f (l acc)
	     (if (and l (cdr l))
		 (f (cddr l) (cons (list (car l) (cadr l)) acc))
		 (reverse acc))))
    (f lst nil)))

;;; this is useful for pedagogic purposes, but I don't know if I would use this in production
;;; this is basically the equivalent for me of the console redirect in shell scripting
;;; so if the file already exists we will blow it the fuck away.
(defmacro redirect-stdout (filename &body body)
  `(with-open-file (*standard-output* ,filename
		    :direction :output
		    :if-exists :supersede)
     ,@body))


(defparameter *bury-pink* '(237 16 232))
(defparameter *nice-blue* '(0 16 255))

;;; my own print-tag function, with blackjack and hookers
;;; it can handle symbols and numbers in the attributes list
(defun print-tag (name attributes closingp)
  ;;let's unpack what's going on here
  ;; enclosing ~( ~) -- print everything lowercase
  ;; enclosing ~:[nil~;t~] -- given a predicate, prints nil case when nil otherwise t case
  ;; ~a -- print the name
  ;; enclosing ~:{ ~} -- destructuring loop -- iterate over a list, destructure sublists
  ;;   as a special note, this requires our alist cells be proper lists rather than dotted
  (format t "~(<~:[~;/~]~a~:{ ~a=\"~a\"~}>~)"
	  closingp name attributes))

#+nil(print-tag "buddy" nil nil)
;;=><buddy>
#+nil(print-tag "pal" nil t)
;;=></pal>
#+nil(print-tag "friendo" '(("a" "1") ("b" "2") (size 500) (girth thick)) nil)
;;=><friendo a="1" b="2" size="500" girth="thick">

;;; we'd like to make something like this:
#+nil(tag mytag (color 'blue height (+ 4 5)))
;;; look like this:
;;; => <mytag color="blue" height="9"></mytag>

;;;notes:
;;;  attributes always come in pairs
;;;  when we open the tag we close the tag
;;;  the tag have nested children, so we close the tag after the body

;; we will need to make a different macro for tags that just contain attrs and no children

;;; macro for printing empty elements -- that is, those which must not contain children
(defmacro etag (name attrs)
  `(print-tag ',name
	     (list ,@(mapcar (lambda (x)
			       `(list ',(first x) ,(second x)))
			     (pairs attrs)))
	     nil))

;;; macro for printing elements with children
(defmacro tag (name attrs &body body)
  `(progn (print-tag ',name
		     ;; this is kind of tricky, but what's going on here is:
		     ;; splice in the mapcar, and then wrap it in a list
		     ;; if we don't splice/wrap it, then at evaluation time
		     ;; it will try and call the list like a function
		     (list ,@(mapcar (lambda (x)
				       ;; for each pair, quote the first element, attr name
				       ;; but don't quote the second element, attr value
				       ;;   instead, evaluate it during macroexpansion
				       ;;   this allows dynamic calculation of attr values
				       `(list ',(first x) ,(second x)))
				     (pairs attrs)))
		     nil)
	  ;; put all the code nested inside the tag macro between opening and closing tags
	  ,@body
	  ;; print the closing tag
	  (print-tag ',name nil t)))

#+nil(tag mytag (color 'blue height (+ 4 5)))
;;=><mytag color="blue" height="9"></mytag>
;; macroexpands to:
;;(PROGN
;;  (PRINT-TAG 'MYTAG
;;	     (LIST (LIST 'COLOR 'BLUE)
;;                         (LIST 'HEIGHT (+ 4 5)))
;;             NIL)
;; (PRINT-TAG 'MYTAG NIL T))

#+nil(tag mytag (color 'red size 'maximum)
  (tag inner-tag-one ())
  (tag inner-tag-two ()))
;; i pretty-formatted this one for my own sake. it just blarts these out on a single line.
;;=><mytag color="red" size="maximum">
;;    <inner-tag-one></inner-tag-one>
;;    <inner-tag-two></inner-tag-two>
;;  </mytag>

#+nil(tag html ()
  (tag head ()
    (tag title () (princ "Test page title")))
  (tag body ()
    (princ "This is a test.")))

;; not too shabby... but what if I don't want to specify nil?
;; what if i need to specify doctype to be html5 compliant?
;;=><html>
;;    <head>
;;      <title>Test page title</title>
;;    </head>
;;    <body>
;;      This is a test.
;;    </body>
;;  </html>

(defmacro html (&body body)
  `(progn (princ "<!DOCTYPE html>")
	  (tag html ()
	    ,@body)))

(defmacro head (title &body body)
  `(tag head ()
     (tag title () (princ ,title))
     ,@body))

(defun stylesheet (href-val)
  (etag link (rel 'stylesheet
		   type "text/css"
		   href href-val)))

#+nil(stylesheet "styles.css")
;;=> <link rel="stylesheet" type="text/css" href="styles.css">

#+nil(head "test page"
  (etag link (rel 'stylesheet
		  type "text/css"
		  href "styles.css")))
;; <head>
;;   <title>test page</title>
;;   <link rel="stylesheet" type="text/css" href="styles.css">
;; </head>NIL

(defmacro body (&body body)
  `(tag body ()
     ,@body))
       

#+nil(html
  (head "testpage"
    (stylesheet "styles.css"))
  (body
    (tag p () (princ "Hello, world!"))))
;;=><!DOCTYPE html>
;;  <html>
;;    <head>
;;      <title>testpage</title>
;;      <link rel="stylesheet" type="text/css" href="styles.css">
;;    </head>
;;    <body>
;;      <p>Hello, world!</p>
;;    </body>
;;  </html>
  
;;; OK -- SVG macro time

(defmacro svg (width height &body body)
  `(tag svg (xmlns "http://www.w3.org/2000/svg"
		   "xmlns:xlink" "http://www.w3.org/1999/xlink"
		   height ,height
		   width ,width)
	,@body))


#+nil(svg 100 100)

;;; color as specified here is a set of 3 integers corresponding to RGB
(defun apply-brightness (color amount)
  (mapcar (lambda (x)
	    (min 255 (max 0 (+ x amount))))
	  color))

(defun svg-style (color)
  (format nil
	  "~{fill:rgb(~a,~a,~a);stroke:rgb(~a,~a,~a)~}"
	  (append color
		  (apply-brightness color -100))))

(svg-style '(237 16 232))
;;=>"fill:rbg(237,16,232);stroke:rbg(137,0,130)"

(defun circle (center radius color)
  (tag circle (cx (first center)
		  cy (second center)
		  r radius
		  style (svg-style color))))

#+nil(circle '(50 50) 50 '(255 0 0))
;;=><circle cx="50" cy="50" r="50" style="fill:rbg(255,0,0);stroke:rbg(155,0,0)"></circle>NIL
#+nil(redirect-stdout "circles.svg"
    (svg 150 150
    (circle '(50 50) 50 *bury-pink*)
    (circle '(100 100) 50 *nice-blue*)))

;;; points is a list of lists of length 2
(defun polygon (points color)
  (tag polygon (points (format nil
			       ;; use the destructuring loop
			       "~:{~a,~a ~}"
			       points)
		       style (svg-style color))))

(defun random-walk (value length amplitude)
  (unless (zerop length)
    (cons value
	  (random-walk (if (zerop (random 2))
			   (- value amplitude)
			   (+ value amplitude))
		       (1- length)
		       amplitude))))

#+nil(random-walk 100 100 10)
;;=>(100 101 102 103 102 103 104 105 104 105 104 103 102 101 102 103 102 101 102
;; 101 102 101 102 103 102 101 102 103 102 101 102 101 100 101 102 101 102 101
;; 100 101 100 101 102 101 100 101 100 99 98 97 98 97 96 95 96 97 98 99 100 101
;; 102 101 102 103 102 101 100 99 100 99 98 97 98 97 96 95 94 95 94 95 94 95 96
;; 97 96 97 96 95 96 97 98 99 100 101 102 103 102 101 100 101)

#+nil
(redirect-stdout "random-walk.svg"
  (svg 400 200
    (loop repeat 10
	  do (polygon (append '((0 200))
			      (loop for x from 1
				    for y in (random-walk 100 400 (1+ (random 3)))
				 collect (list x y))
			      '((400 200)))
			      (loop repeat 3
				    collect (random 256))))))
;;=>NIL
