(defmacro lazy (&body body)
  ;; define some gensyms to use...
  (let ((forced (gensym))
	(value  (gensym)))
    ;; these variables we define here will be captured by our lambda below
    `(let ((,forced nil)
	   (,value nil))
       (lambda ()
	 ;; if we haven't forced evaluation yet, evaluate this:
	 (unless ,forced
	   ;; evaluate body (splice it into a progn), store value
	   (setf ,value (progn ,@body))
	   ;; state that we have forced evaluation of this form
	   (setf ,forced t))
	 ;; reference the stored value
	 ,value))))

(defun force (lazy-value)
  (funcall lazy-value))

#+nil
(let ((l (lazy
	   (princ "adding values now")
	   (+ 6 6 6))))
  (format t "~a~%~a~%" (force l) (force l)))
;;=>adding values now18
;;18
;; fuck yea

(defmacro lazy-cons (a d)
  `(lazy (cons ,a ,d)))

(defun lazy-car (x)
  (car (force x)))

(defun lazy-cdr (x)
  (cdr (force x)))

(defun lazy-nil ()
  (lazy nil))

(defun lazy-null (x)
  (not (force x)))

(defun lazy-cadr (x)
  (lazy-car (lazy-cdr x)))

(defun lazy-caar (x)
  (lazy-car (lazy-car x)))

(defun lazy-cddr (x)
  (lazy-cdr (lazy-cdr x)))

(defun lazy-caddr (x)
  (lazy-cadr (lazy-cdr x)))

(defun lazy-cadddr (x)
  (lazy-caddr (lazy-cdr x)))

(defun lazy-caddddr (x)
  (lazy-caddr (lazy-cdr x)))

;; and so on and so on

(defun make-lazy (lst)
  (lazy (when lst
	  (cons (car lst) (make-lazy (cdr lst))))))

#+nil
(labels ((f (n)
       (lazy-cons n (f (1+ n)))))
  (defparameter *integers* (f 0)))

;;; (take (5 *integers*))
;;(defun take (n lst)

;;;(labels ((f (n)
;;;       (lazy-cons n (f (1+ n)))))
;;;  (defparameter *integers* (f 0))
;;;  (take 10 *integers*)
;;;  ;;; => (0 1 2 3 4 5 6 7 8 9)
(defun take (number lazylist)
  (labels ((f (n ll acc)
	     (cond ((zerop n) acc)
		   ((lazy-null ll) acc)
		   (t (f (1- n) (lazy-cdr ll) (cons (lazy-car ll) acc))))))
    (reverse (f number lazylist nil))))

;;  (cond ((zerop n) nil)
;;	((lazy-null ll) nil)
;;	(t (cons (lazy-car ll) (take (1- n) (lazy-cdr ll))))))
#+nil(labels ((f (n)
       (lazy-cons n (f (1+ n)))))
  (defparameter *integers* (f 0))
  (take 10 *integers*))
;; => (0 1 2 3 4 5 6 7 8 9)

(defun take-all (lazylist)
  (labels ((f (lst acc)
	     (if (lazy-null lst)
		 acc
		 (f (lazy-cdr lst) (cons (lazy-car lst) acc)))))
    (reverse (f lazylist nil))))

#+nil
(take-all (make-lazy '(a b c d e f g h i j k l m n o p q r s t u v w x y z)))
;;=>(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

;;  (if (lazy-null ll)
;;      nil
;;      (cons (lazy-car ll) (take-all (lazy-cdr ll)))))

(defun lazy-mapcar (fun lazylst)
  (lazy (unless (lazy-null lazylst)
	  (cons (funcall fun (lazy-car lazylst))
		(lazy-mapcar fun (lazy-cdr lazylst))))))

#+nil
(labels ((f (n)
       (lazy-cons n (f (1+ n)))))
  (defparameter *integers* (f 0)))
#+nil
(take 10 (lazy-mapcar (lambda (x) (* x x)) *integers*))
;;=>(0 1 4 9 16 25 36 49 64 81)

;; remember this is basically a flatmap
;; where the function we're operating on is going to make a list out of each element
;; and we need to 
(defun lazy-mapcan (fun lazylst)
  (labels ((f (current-lst)
	     (if (lazy-null current-lst)
		 (force (lazy-mapcan fun (lazy-cdr lazylst)))
		 (cons (lazy-car current-lst) (lazy (f (lazy-cdr current-lst)))))))
    (lazy (unless (lazy-null lazylst)
	    (f (funcall fun (lazy-car lazylst)))))))

#+nil
(take 10 (lazy-mapcan (lambda (x)
			(if (evenp x)
			    (make-lazy (list x (* x x)))
			    (lazy nil)))
			*integers*))
;;=>(0 0 2 4 4 16 6 36 8 64)

(defun lazy-find-if (p lst)
  (unless (lazy-null lst)
    (let ((x (lazy-car lst)))
      (if (funcall p x)
	  x
	  (lazy-find-if p (lazy-cdr lst))))))

(defun lazy-remove-if-not (p lst)
  (cond ((lazy-null lst) lst)
	((funcall p (lazy-car lst))
	 (lazy-cons (lazy-car lst) (lazy-remove-if-not p (lazy-cdr lst))))
	(t (lazy-remove-if-not p (lazy-cdr lst)))))

;;    (let ((head (lazy-car lst))
;;	  (tail (lazy-cdr lst)))
;;      (if (funcall p head)
;;	  (cons head (lazy-filter p tail))
;;	  (lazy-filter p tail))))))

#+nil
(lazy-find-if (lambda (n) (= 1000 n)) *integers*)
;; => 1000


(defun lazy-nth (n lst)
  (if (zerop n)
      (lazy-car lst)
      (lazy-nth (1- n) (lazy-cdr lst))))
(defparameter *num-players* 2) ;; number of players
(defparameter *max-dice* 3) ;; maximum number of dice stacked onto a single hex
(defparameter *board-size* 3) ;; number of hexes to a side
(defparameter *board-hexnum* (* *board-size* *board-size*)) ;; a square grid of hexagons...
;; hexagons are the patrician game grid type

;; representing the game board with a simple list,
;; each hexagon is stored in this list, starting at top-left and moving across and then down
;; for each hexagon, we store a list of two items
;;  - A number indicating t

;;he current occupant of the hexagon
;;  - A number indicating the number of dice at the current location

;; Because of our AI player(s), we will need to create a copy of the board as an array
;;   the array will allow very fast lookups...

(defun board-array (lst)
  (make-array *board-hexnum* :initial-contents lst))

(defun generate-board ()
  (board-array (loop for n below *board-hexnum*
		  collect (list (random *num-players*)
				(1+ (random *max-dice*))))))

;; (generate-board)
;; => #((1 3) (1 2) (1 3) (0 1))

(defun player-letter (n)
  (code-char (+ 97 n))) ;; offsets the player number by 97, to get an ascii letter representation

;;(player-letter 0) ;;=>#\a

(defun draw-board (board)
  (loop for y below *board-size*
	do (progn (fresh-line)
		  (loop repeat (- *board-size* y) ;; indents by 2, for each row away from the last there is
			do (princ "  "))
		  (loop for x below *board-size*
			for hex = (aref board (+ x (* *board-size* y)))
			do (format t "~a-~a " (player-letter (first hex))
				              (second hex))))))

;;(draw-board (generate-board))
;; with *board-size* set to 2
;;    b-2 b-2 
;;  b-1 b-1

;; with *board-size* set to 5
;;          a-2 b-1 a-3 a-3 a-2 
;;        a-2 b-1 b-3 b-2 b-2 
;;      a-1 a-1 b-3 b-2 b-1 
;;    b-2 a-1 a-2 a-3 b-1 
;;  a-3 b-3 b-2 b-3 a-1

;; Any computer implementation of a board game needs code to handle human moves,
;;   This code must know the rules and must validate the human's input to ensure their validity

;; The implementation must also include the AI player, which will generate actions based
;;   on the rules as well...

;; Thus, the game engine has three main parts:
;; 1. Handling of human moves
;; 2. The AI player
;; 3. The rule engine, which is used by 1 and 2

;; To do all of this, we will encode the game's rule logic in a lazy game tree <=):^y

;; the game-tree function builds a tree of all possible moves, given a certain starting configuration...
;;   This function will be called only once, at the beginning, recursively calculating all possible moves
;;   the other parts of the game will then traverse this tree in order to conform to the rules of the game
;; we need four pieces of data to do this, representing game-state
;; - current game board
;; - current player
;; - How many dice have been captured by the player in the player's current turn
;;     required to determine the number of dice added as reinforcements to the player's hexes
;; - whether the current move is the first move for the player or not,
;;     required because the player must make at least one move, i.e. they can't pass for their first move
;;(defun game-tree (board player spare-dice first-move)
;;  (list player
;;	board
;;	(add-passing-move board
;;			  player
;;			  spare-dice
;;			  first-move
;;			  (attacking-moves board player spare-dice))))


(let ((f (lambda (board player spare-dice first-move)
	   (list player
		 board
		 (add-passing-move board
				   player
				   spare-dice
				   first-move
				   (attacking-moves board player spare-dice)))))
      (previous (make-hash-table :test #'equalp)))
  (defun game-tree (&rest rest)
    (or (gethash rest previous)
	(setf (gethash rest previous) (apply f rest)))))


;; ---------
;;|root:    |-move1,,,
;;|player # |-move2,,,
;;|board ...|-move3,,,
;;-----------
;; (player board moves)

;; The structure of a move is thus:
;; - a description of a move, either nil or <i dont know yet>
;; - an entirely new game tree, which holds the entire universe of valid moves that exist after this move.

;; dummy funcs
;; There are two types of legal moves possible for players,
;;  - Attack a hexagon,
;;  - pass their turn, iff it's not their first move
#+nil
(defun add-passing-move (board player spare-dice first-move moves)
  (if first-move ;; if this is the first move, then we cannot add a passing move to the list of moves
      moves ;; simply return the list of opening moves
      (cons (list nil ;; append passing turn here, nil and an updated game grid
		  (game-tree (add-new-dice board player (1- spare-dice)) ;; Adds reinforcements 
			     (mod (1+ player) *num-players*) ;; rolls the turn over to the next player, mod n
			     0 ;; states that there are currently no captured dice
			     t)) ;; states that the next set of moves to add will be opening moves...
	    moves)))

;; let's think about what attacking moves are...
;; given a position on the game board: p
;;  1. p must have at least 2 dice
;; we can say that a valid position to attack is:
;;  1. a neighbor of p
;;  2. A different color (owned by a different player than) p
;;  3. Has less dice than the dice at p
;; so, a valid move is one that moves all but one die from a position to a valid attack position

;; generates the attacking moves for a given board state and player, tracking spare-dice
#+nil
(defun attacking-moves (board cur-player spare-dice)
  (labels ((player (pos) ;; retrieves the player at a position
	     (car (aref board pos)))
	   (dice (pos) ;; retrieves the dice count at a position
	     (cadr (aref board pos))))
    (mapcan (lambda (src) ;;  iterate over sources
	      (when (eq (player src) cur-player) ;; when src position is current player's
		(mapcan (lambda (dst) ;; iterate over destinations
			  (when (and (not (eq (player dst) cur-player)) ;; is valid pos-to-attack
				     (> (dice src) (dice dst)))
			    (list
			     ;; move is: ((src dest) attack-game-tree)
			     (list (list src dst)
				   (game-tree (board-attack board cur-player src dst (dice src))
					      cur-player
					      (+ spare-dice (dice dst))
					      nil)))))
			(neighbors src))))
	    (loop for n below *board-hexnum*
		  collect n))))



;; use `f` to memoize the neighbors function
(let ((f (lambda (pos)
	   (let ((up (- pos *board-size*))
	    (down (+ pos *board-size*)))
	(loop for p in (append (list up down) ;; generate a list of possible neighbors, relative to edges
			    (unless (zerop (mod pos *board-size*)) ;; determines if pos is on left-edge of board
				(list (1- up) (1- pos))) ;; gets left-wise positions of up and position
			    (unless (zerop (mod (1+ pos) *board-size*))
				(list (1+ down) (1+ pos))))
	    when (and (>= p 0) (< p *board-hexnum*))
	      collect p))))
      (previous (make-hash-table)))
  (defun neighbors (pos)
    (or (gethash pos previous)
	(setf (gethash pos previous) (funcall f pos)))))

;;(defun neighbors (pos)
;;    (let ((up (- pos *board-size*))
;;	    (down (+ pos *board-size*)))
;;	(loop for p in (append (list up down) ;; generate a list of possible neighbors, relative to edges
;;			    (unless (zerop (mod pos *board-size*)) ;; determines if pos is on left-edge of board
;;				(list (1- up) (1- pos))) ;; gets left-wise positions of up and position
;;			    (unless (zerop (mod (1+ pos) *board-size*))
;;				(list (1+ down) (1+ pos))))
;;	    when (and (>= p 0) (< p *board-hexnum*))
;;		collect p)))

;; remember that each hex contains the data (player dice)
;; makes a new board array with the outcome of the attack from src to dest
(defun board-attack (board player src dst dice)
  (board-array (loop for pos from 0 
		     for hex across board
		     collect (cond ((eq pos src) (list player 1)) ;; src dest belongs to player, 1 die
				   ((eq pos dst) (list player (1- dice)))
				   (t hex)))))

;;(board-attack #((0 3) (0 3) (1 3) (1 1)) 0 1 3 3)
;; => #((0 3) (0 1) (1 3) (0 2))

(defun add-new-dice (board player spare-dice)
  (labels ((f (lst n acc) ;; lst is board, n is number of dice remaining
	     ;;(format t "list is ~a , spare dice is ~a~%" lst n)
	     (cond ((null lst) (reverse acc)) ;; we have recurred through the entire board
		   ((zerop n) (append (reverse acc) lst))  ;; we have assigned all reinforcements
		   (t (let ((cur-player (caar lst)) ;; player at pos
			    (cur-dice (cadar lst))) ;; dice at pos
			(if (and (eq cur-player player) (< cur-dice *max-dice*)) ;; player at pos is current player, and the dice at pos is less than the maximum permitted dice:
			    (f (cdr lst)
			       (1- n)
			       (cons (list cur-player (1+ cur-dice)) acc))
			    ;;(cons (list cur-player (1+ cur-dice)) ;; increment dice at hex
				;;  (f (cdr lst) (1- n))) ;; and recur with num-dice decremented
			    (f (cdr lst) n (cons (car lst) acc))))))))
			    ;;(cons (car lst) (f (cdr lst) n)))))))) ;; otherwise just leave it alone, and recur on the rest of the array
    (board-array (f (coerce board 'list) spare-dice '())))) ;; coerce the board into a list, and call f on it

;;(add-new-dice #((0 1) (0 2) (0 3) (1 1)) 0 3)
;; => #((0 2) (0 3) (0 3) (1 1))

;;(add-new-dice #((1 1) (0 2) (0 1) (1 1)) 0 1)
;; => #((1 1) (0 3) (0 1) (1 1))

;;(loop for i below *board-hexnum*
;;     collect (list i (neighbors i)))

;;    0 1 2
;;   3 4 5
;;  6 7 8
;;=>((0 (3 4 1)) (1 (4 0 5 2)) (2 (5 1)) (3 (0 6 7 4)) (4 (1 7 0 3 8 5))
;;  (5 (2 8 1 4)) (6 (3 7)) (7 (4 3 6 8)) (8 (5 4 7)))

;;  0 1
;; 2 3 
;; => ((0 (2 3 1)) (1 (3 0)) (2 (0 3)) (3 (1 0 2)))

;;(game-tree #((0 1) (1 1) (0 2) (1 1)) 0 0 t)
;;(0
;;  #((0 1) (1 1) (0 2) (1 1))
;;  (((2 3) (0
;;             #((0 1) (1 1) (0 1) (0 1))
;;             ((NIL (1
;;                      #((0 1) (1 1) (0 1) (0 1))
;;                      NIL)))))))

;;(defun get-player (tree)
;;  (car tree))
;;
;;(defun get-board (tree)
;;  (cadr tree))
;;
;;(defun get-moves (tree)
;;  (caddr tree))

;; structure of the game tree:
;; (player board moves)

;; structure of a move:
;; ((src dest) (resulting-tree))
;; (nil (resulting-tree))

#+nil
(defun play-vs-human (tree)
  (print-info tree)
  (if (caddr tree)
      (play-vs-human (handle-human tree))
      (announce-winner (cadr tree))))

(defun print-info (tree)
  (fresh-line)
  (format t "current player = ~a" (player-letter (car tree)))
  (draw-board (cadr tree)))

#+nil
(defun handle-human (tree)
  (fresh-line)
  (princ "Choose your move:")
  (let ((moves (caddr tree))) 
    (loop for move in moves ;; for each move
	  for n from 1      ;; tracking move number as well...
	  do (let ((action (car move))) ;; action is the car of a move, followed by the tree
	       (fresh-line)
	       (format t "~a. " n)
	       (if action ;; action is either (src dest) or nil
		   (format t "~a -> ~a" (car action) (cadr action)) ;; print out movement
		   (princ "end turn")))) ;; pass turn
    (fresh-line)
    (cadr (nth (1- (read)) moves)))) ;; read in the move from the player... I don't really like this.

(defun winners (board)
  (let* ((tally (loop for hex across board
		      collect (car hex))) ;; collect the players across the board
	 (totals (mapcar (lambda (player) ;; build an alist of (player . controlled)
			   (cons player (count player tally))) ;; create the association
			 (remove-duplicates tally))) ;; get a set of players still on the board
	 (best (apply #'max (mapcar #'cdr totals)))) ;; get the highest score in the list
    (mapcar #'car
	    (remove-if (lambda (x) ;; kick out any player that doesn't have the best score.
			 (not (eq (cdr x) best)))
		       totals))))

(defun announce-winner (board)
  (fresh-line)
  (let ((winning-players (mapcar #'player-letter (winners board)))) ;; get the winners out of the board
    (if (> (length winning-players) 1) ;; check if there's one winner or if it's a tie
	(format t "the game is a tie between ~a ~{and ~a~}"
		(car winning-players) (cdr winning-players))
	(format t "the winner is ~a" (car winning-players)))))

;; now let's implement minimax!

;;(defun rate-position (tree player)
#+nil
(let ((f (lambda (tree player)
	   (let ((moves (caddr tree)))
	     (if moves
		 (apply (if (eq (car tree) player)
			    #'max  ;; get maximum scoring move in context of moves for THIS player
			    #'min) ;; get minimum scoring move in context of moves for THAT player
			(get-ratings tree player))
		 (let ((winning-players (winners (cadr tree))))
		   (if (member player winning-players)
		       (/ 1 (length winning-players))
		       0))))))
      (previous (make-hash-table)))
  (defun rate-position (tree player)
    (let ((tab (gethash player previous))) ;; looks up player in the hash table, using eql
      (unless tab
	(setf tab (setf (gethash player previous) (make-hash-table)))) ;; create hash table for player
      (or (gethash tree tab)
	  (setf (gethash tree tab)
		(funcall f tree player))))))

(defun get-ratings (tree player)
  (mapcar (lambda (move) ;; map rate-position over all moves of the current tree
	    (rate-position (cadr move) player))
	  (caddr tree)))

;; gets the next move for the AI player based on the current tree
#+nil
(defun handle-computer (tree)
  ;; list of ratings for moves
  (let ((ratings (get-ratings tree (car tree))))
    ;; get the position of the first highest rated move, and select the tree corresponding to that move
    (cadr (nth (position (apply #'max ratings) ratings) (caddr tree)))))

#+nil
(defun play-vs-computer (tree)
  (print-info tree)
  (cond ((null (caddr tree)) (announce-winner (cadr tree)))
	((zerop (car tree)) (play-vs-computer (handle-human tree)))
	(t (play-vs-computer (handle-computer tree)))))

(defparameter *board-size* 5)
(defparameter *ai-level* 4)
(defparameter *board-hexnum* (* *board-size* *board-size*))

(defun new-game () (game-tree (generate-board) 0 0 t))


;;; the list of passing moves has been made into a lazy list
;;; the whole damn game turns into a lazily evaluated game
;;; this one change will require us changing a multitude of other effects.
(defun add-passing-move (board player spare-dice first-move moves)
  (if first-move
      moves
      (lazy-cons (list nil
		       (game-tree (add-new-dice board player
						(1- spare-dice))
				  (mod (1+ player) *num-players*)
				  0
				  t))
		 moves)))

(defun attacking-moves (board cur-player spare-dice)
  (labels ((player (pos)
	     (car (aref board pos)))
	   (dice (pos)
	     (cadr (aref board pos))))
    (lazy-mapcan
     (lambda (src)
       (if (eq (player src) cur-player)
	   (lazy-mapcan
	    (lambda (dst)
	      (if (and (not (eq (player dst)
				cur-player))
		       (> (dice src) (dice dst)))
		  (make-lazy
		   (list (list (list src dst)
			       (game-tree (board-attack board
							cur-player
							src
							dst
							(dice src))
					  cur-player
					  (+ spare-dice (dice dst))
					  nil))))
		  (lazy-nil)))
	    (make-lazy (neighbors src)))
	   (lazy-nil)))
     (make-lazy (loop for n below *board-hexnum*
		      collect n)))))
	   
(defun handle-human (tree)
  (fresh-line)
  (princ "Choose you move:")
  (let ((moves (caddr tree))) ; list of moves is lazy
    (labels ((print-moves (moves n)
	       (unless (lazy-null moves)
		 (let* ((move (lazy-car moves))
			(action (car move)))
		   (fresh-line)
		   (format t "~a. " n)
		   (if action
		       (format t "~a -> ~a" (car action) (cadr action))
		       (princ "end turn")))
		 (print-moves (lazy-cdr moves) (1+ n)))))
      (print-moves moves 1))
    (fresh-line)
    (cadr (lazy-nth (1- (read)) moves))))

(defun play-vs-human (tree)
  (print-info tree)
  (if (not (lazy-null (caddr tree)))
      (play-vs-human (handle-human tree))
      (announce-winner (cadr tree))))


;; produces a lazy-tree trimmed to a given depth
(defun limit-tree-depth (tree depth)
  (list (car tree)
	(cadr tree)
	(if (zerop depth)
	    (lazy-nil)
	    (lazy-mapcar (lambda (move)
			   (list (car move)
				 (limit-tree-depth (cadr move) (1- depth))))
			 (caddr tree)))))

;;; this new version of handle-computer is working with a tree with a max depth of *ai-level*
;;; get-ratings isn't implemented to handle a lazy-list yet
#+nil
(defun handle-computer (tree)
  (let ((ratings (get-ratings (limit-tree-depth tree *ai-level*)
			      (car tree))))
    (cadr (lazy-nth (position (apply #'max ratings) ratings)
		    (caddr tree)))))

(defun play-vs-computer (tree)
  (print-info tree)
  (cond ((lazy-null (caddr tree)) (announce-winner (cadr tree)))
	((zerop (car tree)) (play-vs-computer (handle-human tree)))
	(t (play-vs-computer (handle-computer tree)))))

(defparameter *threatened-pos-score* 1)
(defparameter *secure-pos-score* 2)
(defparameter *enemy-pos-score* -1)

(defun score-board (board player)
  (loop for hex across board
	for pos from 0
	sum (if (eq (car hex) player)
		(if (threatened pos board)
		    *threatened-pos-score*
		    *secure-pos-score*)
		*enemy-pos-score*)))

(defun threatened (pos board)
  (let* ((hex (aref board pos))
	 (player (car hex))
	 (dice (cadr hex)))
    (loop for n in (neighbors pos)
	  do (let* ((nhex (aref board n))
		    (nplayer (car nhex))
		    (ndice (cadr nhex)))
	       ;; when the neighboring hex is an enemy that has more dice than this pos
	       (when (and (not (eq player nplayer)) (> ndice dice))
		 ;; this return statement here breaks us out of the loop early
		 (return t)))))) 

#+nil
(defun get-ratings (tree player)
  ;; take-all forces strict evaluation of lazy-mapcar
  (take-all (lazy-mapcar (lambda (move)
			   (rate-position (cadr move) player))
			 (caddr tree))))

(defun rate-position (tree player)
  (let ((moves (caddr tree)))
    (if (not (lazy-null moves))
	;; if there are moves, then evaluate them and select the one that scores the best for us
	(apply (if (eq (car tree) player)
		   #'max
		   #'min)
	       (get-ratings tree player))
	;; otherwise, score the board and return that
	(score-board (cadr tree) player))))


(defun ab-get-ratings-max (tree player upper-limit lower-limit)
  (labels ((f (moves lower-limit)
	     (unless (lazy-null moves)
	       (let ((x (ab-rate-position (cadr (lazy-car moves))
					  player
					  upper-limit
					  lower-limit)))
		 (if (>= x upper-limit)
		     (list x)
		     (cons x (f (lazy-cdr moves) (max x lower-limit))))))))
    (f (caddr tree) lower-limit)))


(defun ab-get-ratings-min (tree player upper-limit lower-limit)
  (labels ((f (moves upper-limit)
	     (unless (lazy-null moves)
	       ;; get the score of the tree's position
	       (let ((x (ab-rate-position (cadr (lazy-car moves))
					  player
					  upper-limit
					  lower-limit)))
		 ;; if x, the score is less than the lower-limit
		 (if (<= x lower-limit)
		     ;; we, the minimizing player, will select this score because it is under the lower limit.
		     (list x)
		     ;; otherwise, we're still looking for a lower score, since we're trying to minimize it.
		     ;; note also that we update the upper-limit to the minimum of this score and the current.
		     (cons x (f (lazy-cdr moves) (min x upper-limit))))))))
    (f (caddr tree) upper-limit)))

(defun ab-rate-position (tree player upper-limit lower-limit)
  (let ((moves (caddr tree)))
    (if (not (lazy-null moves))
	;; if we're rating relative to the current player (i.e. trying to maximize score
	(if (eq (car tree) player)
	    ;; select the maximum value of the moves available to us
	    (apply #'max (ab-get-ratings-max tree
					     player
					     upper-limit
					     lower-limit))
	    ;; however, if we're rating a move for the opposing player
	    ;;   we want to select the minimally scoring move -- basically
	    ;;   we're selecting as if the opposing player was playing their best
	    ;;   and preventing us from getting a good score
	    (apply #'min (ab-get-ratings-min tree
					     player
					     upper-limit
					     lower-limit)))
	;; static board score
	(score-board (cadr tree) player))))

(defun handle-computer (tree)
  (let ((ratings (ab-get-ratings-max (limit-tree-depth tree *ai-level*)
				     (car tree)
				     most-positive-fixnum
				     most-negative-fixnum)))
    (cadr (lazy-nth (position (apply #'max ratings) ratings) (caddr tree)))))
