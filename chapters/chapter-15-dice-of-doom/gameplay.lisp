(load "dice-of-doom.lisp")

(defun play-vs-human (tree)
  (print-info tree)
  (if (caddr tree)
      (play-vs-human (handle-human tree))
      (announce-winner (cadr tree))))

(defun print-info (tree)
  (fresh-line)
  (format t "current player = ~a" (player-letter (car tree)))
  (draw-board (cadr tree)))

(defun handle-human (tree)
  (fresh-line)
  (princ "Choose your move:")
  (let ((moves (caddr tree))) 
    (loop for move in moves ;; for each move
	  for n from 1      ;; tracking move number as well...
	  do (let ((action (car move))) ;; action is the car of a move, followed by the tree
	       (fresh-line)
	       (format t "~a. " n)
	       (if action ;; action is either (src dest) or nil
		   (format t "~a -> ~a" (car action) (cadr action)) ;; print out movement
		   (princ "end turn")))) ;; pass turn
    (fresh-line)
    (cadr (nth (1- (read)) moves)))) ;; read in the move from the player... I don't really like this.

(defun winners (board)
  (let* ((tally (loop for hex across board
		      collect (car hex))) ;; collect the players across the board
	 (totals (mapcar (lambda (player) ;; build an alist of (player . controlled)
			   (cons player (count player tally))) ;; create the association
			 (remove-duplicates tally))) ;; get a set of players still on the board
	 (best (apply #'max (mapcar #'cdr totals)))) ;; get the highest score in the list
    (mapcar #'car
	    (remove-if (lambda (x) ;; kick out any player that doesn't have the best score.
			 (not (eq (cdr x) best)))
		       totals))))

(defun announce-winner (board)
  (fresh-line)
  (let ((winning-players (mapcar #'player-letter (winners board)))) ;; get the winners out of the board
    (if (> (length winning-players) 1) ;; check if there's one winner or if it's a tie
	(format t "the game is a tie between ~a ~{and ~a~}"
		(car winning-players) (cdr winning-players))
	(format t "the winner is ~a" (car winning-players)))))

;; now let's implement minimax!

;;(defun rate-position (tree player)
(let ((f (lambda (tree player)
	   (let ((moves (caddr tree)))
	     (if moves
		 (apply (if (eq (car tree) player)
			    #'max  ;; get maximum scoring move in context of moves for THIS player
			    #'min) ;; get minimum scoring move in context of moves for THAT player
			(get-ratings tree player))
		 (let ((winning-players (winners (cadr tree))))
		   (if (member player winning-players)
		       (/ 1 (length winning-players))
		       0))))))
      (previous (make-hash-table)))
  (defun rate-position (tree player)
    (let ((tab (gethash player previous))) ;; looks up player in the hash table, using eql
      (unless tab
	(setf tab (setf (gethash player previous) (make-hash-table)))) ;; create hash table for player
      (or (gethash tree tab)
	  (setf (gethash tree tab)
		(funcall f tree player))))))

(defun get-ratings (tree player)
  (mapcar (lambda (move) ;; map rate-position over all moves of the current tree
	    (rate-position (cadr move) player))
	  (caddr tree)))

;; gets the next move for the AI player based on the current tree
(defun handle-computer (tree)
  ;; list of ratings for moves
  (let ((ratings (get-ratings tree (car tree))))
    ;; get the position of the first highest rated move, and select the tree corresponding to that move
    (cadr (nth (position (apply #'max ratings) ratings) (caddr tree)))))

(defun play-vs-computer (tree)
  (print-info tree)
  (cond ((null (caddr tree)) (announce-winner (cadr tree)))
	((zerop (car tree)) (play-vs-computer (handle-human tree)))
	(t (play-vs-computer (handle-computer tree)))))
