(defparameter *num-players* 2) ;; number of players
(defparameter *max-dice* 3) ;; maximum number of dice stacked onto a single hex
(defparameter *board-size* 3) ;; number of hexes to a side
(defparameter *board-hexnum* (* *board-size* *board-size*)) ;; a square grid of hexagons...
;; hexagons are the patrician game grid type

;; representing the game board with a simple list,
;; each hexagon is stored in this list, starting at top-left and moving across and then down
;; for each hexagon, we store a list of two items
;;  - A number indicating t

;;he current occupant of the hexagon
;;  - A number indicating the number of dice at the current location

;; Because of our AI player(s), we will need to create a copy of the board as an array
;;   the array will allow very fast lookups...

(defun board-array (lst)
  (make-array *board-hexnum* :initial-contents lst))

(defun generate-board ()
  (board-array (loop for n below *board-hexnum*
		  collect (list (random *num-players*)
				(1+ (random *max-dice*))))))

;; (generate-board)
;; => #((1 3) (1 2) (1 3) (0 1))

(defun player-letter (n)
  (code-char (+ 97 n))) ;; offsets the player number by 97, to get an ascii letter representation

;;(player-letter 0) ;;=>#\a

(defun draw-board (board)
  (loop for y below *board-size*
	do (progn (fresh-line)
		  (loop repeat (- *board-size* y) ;; indents by 2, for each row away from the last there is
			do (princ "  "))
		  (loop for x below *board-size*
			for hex = (aref board (+ x (* *board-size* y)))
			do (format t "~a-~a " (player-letter (first hex))
				              (second hex))))))

;;(draw-board (generate-board))
;; with *board-size* set to 2
;;    b-2 b-2 
;;  b-1 b-1

;; with *board-size* set to 5
;;          a-2 b-1 a-3 a-3 a-2 
;;        a-2 b-1 b-3 b-2 b-2 
;;      a-1 a-1 b-3 b-2 b-1 
;;    b-2 a-1 a-2 a-3 b-1 
;;  a-3 b-3 b-2 b-3 a-1

;; Any computer implementation of a board game needs code to handle human moves,
;;   This code must know the rules and must validate the human's input to ensure their validity

;; The implementation must also include the AI player, which will generate actions based
;;   on the rules as well...

;; Thus, the game engine has three main parts:
;; 1. Handling of human moves
;; 2. The AI player
;; 3. The rule engine, which is used by 1 and 2

;; To do all of this, we will encode the game's rule logic in a lazy game tree <=):^y

;; the game-tree function builds a tree of all possible moves, given a certain starting configuration...
;;   This function will be called only once, at the beginning, recursively calculating all possible moves
;;   the other parts of the game will then traverse this tree in order to conform to the rules of the game
;; we need four pieces of data to do this, representing game-state
;; - current game board
;; - current player
;; - How many dice have been captured by the player in the player's current turn
;;     required to determine the number of dice added as reinforcements to the player's hexes
;; - whether the current move is the first move for the player or not,
;;     required because the player must make at least one move, i.e. they can't pass for their first move
;;(defun game-tree (board player spare-dice first-move)
;;  (list player
;;	board
;;	(add-passing-move board
;;			  player
;;			  spare-dice
;;			  first-move
;;			  (attacking-moves board player spare-dice))))


(let ((f (lambda (board player spare-dice first-move)
	   (list player
		 board
		 (add-passing-move board
				   player
				   spare-dice
				   first-move
				   (attacking-moves board player spare-dice)))))
      (previous (make-hash-table :test #'equalp)))
  (defun game-tree (&rest rest)
    (or (gethash rest previous)
	(setf (gethash rest previous) (apply f rest)))))


;; ---------
;;|root:    |-move1,,,
;;|player # |-move2,,,
;;|board ...|-move3,,,
;;-----------
;; (player board moves)

;; The structure of a move is thus:
;; - a description of a move, either nil or <i dont know yet>
;; - an entirely new game tree, which holds the entire universe of valid moves that exist after this move.

;; dummy funcs
;; There are two types of legal moves possible for players,
;;  - Attack a hexagon,
;;  - pass their turn, iff it's not their first move
(defun add-passing-move (board player spare-dice first-move moves)
  (if first-move ;; if this is the first move, then we cannot add a passing move to the list of moves
      moves ;; simply return the list of opening moves
      (cons (list nil ;; append passing turn here, nil and an updated game grid
		  (game-tree (add-new-dice board player (1- spare-dice)) ;; Adds reinforcements 
			     (mod (1+ player) *num-players*) ;; rolls the turn over to the next player, mod n
			     0 ;; states that there are currently no captured dice
			     t)) ;; states that the next set of moves to add will be opening moves...
	    moves)))

;; let's think about what attacking moves are...
;; given a position on the game board: p
;;  1. p must have at least 2 dice
;; we can say that a valid position to attack is:
;;  1. a neighbor of p
;;  2. A different color (owned by a different player than) p
;;  3. Has less dice than the dice at p
;; so, a valid move is one that moves all but one die from a position to a valid attack position

;; generates the attacking moves for a given board state and player, tracking spare-dice
(defun attacking-moves (board cur-player spare-dice)
  (labels ((player (pos) ;; retrieves the player at a position
	     (car (aref board pos)))
	   (dice (pos) ;; retrieves the dice count at a position
	     (cadr (aref board pos))))
    (mapcan (lambda (src) ;;  iterate over sources
	      (when (eq (player src) cur-player) ;; when src position is current player's
		(mapcan (lambda (dst) ;; iterate over destinations
			  (when (and (not (eq (player dst) cur-player)) ;; is valid pos-to-attack
				     (> (dice src) (dice dst)))
			    (list
			     ;; move is: ((src dest) attack-game-tree)
			     (list (list src dst)
				   (game-tree (board-attack board cur-player src dst (dice src))
					      cur-player
					      (+ spare-dice (dice dst))
					      nil)))))
			(neighbors src))))
	    (loop for n below *board-hexnum*
		  collect n))))



;; use `f` to memoize the neighbors function
(let ((f (lambda (pos)
	   (let ((up (- pos *board-size*))
	    (down (+ pos *board-size*)))
	(loop for p in (append (list up down) ;; generate a list of possible neighbors, relative to edges
			    (unless (zerop (mod pos *board-size*)) ;; determines if pos is on left-edge of board
				(list (1- up) (1- pos))) ;; gets left-wise positions of up and position
			    (unless (zerop (mod (1+ pos) *board-size*))
				(list (1+ down) (1+ pos))))
	    when (and (>= p 0) (< p *board-hexnum*))
	      collect p))))
      (previous (make-hash-table)))
  (defun neighbors (pos)
    (or (gethash pos previous)
	(setf (gethash pos previous) (funcall f pos)))))

;;(defun neighbors (pos)
;;    (let ((up (- pos *board-size*))
;;	    (down (+ pos *board-size*)))
;;	(loop for p in (append (list up down) ;; generate a list of possible neighbors, relative to edges
;;			    (unless (zerop (mod pos *board-size*)) ;; determines if pos is on left-edge of board
;;				(list (1- up) (1- pos))) ;; gets left-wise positions of up and position
;;			    (unless (zerop (mod (1+ pos) *board-size*))
;;				(list (1+ down) (1+ pos))))
;;	    when (and (>= p 0) (< p *board-hexnum*))
;;		collect p)))

;; remember that each hex contains the data (player dice)
;; makes a new board array with the outcome of the attack from src to dest
(defun board-attack (board player src dst dice)
  (board-array (loop for pos from 0 
		     for hex across board
		     collect (cond ((eq pos src) (list player 1)) ;; src dest belongs to player, 1 die
				   ((eq pos dst) (list player (1- dice)))
				   (t hex)))))

;;(board-attack #((0 3) (0 3) (1 3) (1 1)) 0 1 3 3)
;; => #((0 3) (0 1) (1 3) (0 2))

(defun add-new-dice (board player spare-dice)
  (labels ((f (lst n acc) ;; lst is board, n is number of dice remaining
	     ;;(format t "list is ~a , spare dice is ~a~%" lst n)
	     (cond ((null lst) (reverse acc)) ;; we have recurred through the entire board
		   ((zerop n) (append (reverse acc) lst))  ;; we have assigned all reinforcements
		   (t (let ((cur-player (caar lst)) ;; player at pos
			    (cur-dice (cadar lst))) ;; dice at pos
			(if (and (eq cur-player player) (< cur-dice *max-dice*)) ;; player at pos is current player, and the dice at pos is less than the maximum permitted dice:
			    (f (cdr lst)
			       (1- n)
			       (cons (list cur-player (1+ cur-dice)) acc))
			    ;;(cons (list cur-player (1+ cur-dice)) ;; increment dice at hex
				;;  (f (cdr lst) (1- n))) ;; and recur with num-dice decremented
			    (f (cdr lst) n (cons (car lst) acc))))))))
			    ;;(cons (car lst) (f (cdr lst) n)))))))) ;; otherwise just leave it alone, and recur on the rest of the array
    (board-array (f (coerce board 'list) spare-dice '())))) ;; coerce the board into a list, and call f on it

;;(add-new-dice #((0 1) (0 2) (0 3) (1 1)) 0 3)
;; => #((0 2) (0 3) (0 3) (1 1))

;;(add-new-dice #((1 1) (0 2) (0 1) (1 1)) 0 1)
;; => #((1 1) (0 3) (0 1) (1 1))

;;(loop for i below *board-hexnum*
;;     collect (list i (neighbors i)))

;;    0 1 2
;;   3 4 5
;;  6 7 8
;;=>((0 (3 4 1)) (1 (4 0 5 2)) (2 (5 1)) (3 (0 6 7 4)) (4 (1 7 0 3 8 5))
;;  (5 (2 8 1 4)) (6 (3 7)) (7 (4 3 6 8)) (8 (5 4 7)))

;;  0 1
;; 2 3 
;; => ((0 (2 3 1)) (1 (3 0)) (2 (0 3)) (3 (1 0 2)))

;;(game-tree #((0 1) (1 1) (0 2) (1 1)) 0 0 t)
;;(0
;;  #((0 1) (1 1) (0 2) (1 1))
;;  (((2 3) (0
;;             #((0 1) (1 1) (0 1) (0 1))
;;             ((NIL (1
;;                      #((0 1) (1 1) (0 1) (0 1))
;;                      NIL)))))))

;;(defun get-player (tree)
;;  (car tree))
;;
;;(defun get-board (tree)
;;  (cadr tree))
;;
;;(defun get-moves (tree)
;;  (caddr tree))

;; structure of the game tree:
;; (player board moves)

;; structure of a move:
;; ((src dest) (resulting-tree))
;; (nil (resulting-tree))
