(defparameter *width* 100)
(defparameter *height* 100)
(defparameter *jungle* `(,(- (ash *height* -1) 10) 10 10 10))
(defparameter *plant-energy* 100)
(defparameter *plants* (make-hash-table :test #'equal))
(defparameter *reproduction-energy* 200)


(defun random-plant (left top width height)
  (let ((pos (cons (+ left (random width)) (+ top (random height)))))
    (setf (gethash pos *plants*) t)))

;; adds a plant to the world map and the jungle
(defun add-plants ()
  (apply #'random-plant *jungle*)
  (random-plant 0 0 *width* *height*))

;; x -- x coordinate in the game grid
;; y -- y coordinate in the game grid
;; energy -- days (turns) of energy remaining
;; dir -- direction the animal is facing
;; genes -- list of weights, determining which direction the animal will turn
(defstruct animal x y energy dir genes)

;; list position mapped to game-grid direction
;; 0 1 2
;; 7   3
;; 6 5 4
;; (0  1  2  3  4  5  6  7)
;; (NW N  NE E  SE S  SW W)

(defparameter *animals*
  (list (make-animal :x      (ash *width*  -1)
		     :y      (ash *height* -1)
		     :energy 1000
		     :dir    (random 8)
		     :genes  (loop repeat 8
				  collect (1+ (random 10))))))

(defun move (animal)
  (let ((dir (animal-dir animal))
	(x (animal-x animal))
	(y (animal-y animal)))
    (setf (animal-x animal) (mod (+ x
				    (cond ((and (>= dir 2) (< dir 5)) 1) ;; if direction points to the east at all, increment
					  ((or (= dir 1) (= dir 5)) 0)   ;; if the direction points just north or south, do nothing
					  (t -1)))                       ;; otherwise, it must point west, so decrement
				 *width*)) ;; apply the operation modulo *width*, so we wrap around to the other side
    (setf (animal-y animal) (mod (+ y
				    (cond ((and (>= 0) (< dir 3)) -1)
					  ((and (>= dir 4) (< dir 7)) 1)
					  (t 0)))                       
				 *height*))
    (decf (animal-energy animal)))) ;; motion do be requiring a little bit of energy

(defun turn (animal)
  (let ((x (random (apply #'+ (animal-genes animal))))) ;; x is a random int between 0 and (sum genes)
    (labels ((angle (genes x)                           ;; genes is the list of weights we are recursing through
	       (let ((xnu (- x (car genes))))           ;; xnu is the result of subtracting x by the head of the list of weights
		 (if (< xnu 0)                          ;; if x is less than 0, 
		     0                                  ;; then return a 0 to indicate it falls inside *this* direction
		     (1+ (angle (cdr genes) xnu))))))  ;; else add a 1 to x and then iterate using (cdr genes) and xnu
	(setf (animal-dir animal)
	    (mod (+ (animal-dir animal)              ;; add current animal direction
		    (angle (animal-genes animal) x)) ;; to the angle derived from the animal's genes today
		8)))))                               ;; modulo 8, since the highest our angle can be is 7

(defun eat (animal)
  (let ((pos (cons (animal-x animal) (animal-y animal)))) ;; a pos is a cons-cell (x . y)
    (when (gethash pos *plants*)                   ;; when there is a plant at current position
      (incf (animal-energy animal) *plant-energy*) ;; increase the animal's energy by set *plant-energy* amount
      (remhash pos *plants*))))                    ;; and fucking remove the plant from the game grid


(defun reproduce (animal)
  (let ((e (animal-energy animal)))
    (when (>= e *reproduction-energy*)
      (setf (animal-energy animal) (ash e -1))
      (let ((animal-nu (copy-structure animal))           ;; copy-structure does a shallow copy
	    (genes     (copy-list (animal-genes animal))) ;; so we have to do a deep copy of the list w/ copy-list
	    (mutation  (random 8)))
	(setf (nth mutation genes) ;; set the nth gene
	      (max 1 (+ (nth mutation genes) (random 5) -2))) ;; mutate the gene by [-2, 2], leave the weight at 1
	(setf (animal-genes animal-nu) genes)
	(push animal-nu *animals*)))))

(defun animal-activity (animal)
  (turn animal)
  (move animal)
  (eat animal)
  (reproduce animal))

(defun update-world ()
  (setf *animals* (remove-if (lambda (animal)
			       (<= (animal-energy animal) 0))
			     *animals*))
  (mapc #'animal-activity *animals*)
  (add-plants))

(defun draw-animal (animal)
  (let ((dir (animal-dir animal)))
    (cond ((= dir 0) #\NORTH_WEST_ARROW)
	  ((= dir 1) #\UPWARDS_ARROW)
	  ((= dir 2) #\NORTH_EAST_ARROW)
	  ((= dir 3) #\RIGHTWARDS_ARROW)
	  ((= dir 4) #\SOUTH_EAST_ARROW)
	  ((= dir 5) #\DOWNWARDS_ARROW)
	  ((= dir 6) #\SOUTH_WEST_ARROW)
	  ((= dir 7) #\LEFTWARDS_ARROW))))

(defun draw-world ()
  (loop for y
	  below *height*
	do (progn (fresh-line)
		  (princ "|")
		  (loop for x below *width*
				    do (princ (cond ((some (lambda (animal)
							      (and (= (animal-x animal) x)
								   (= (animal-y animal) y)))
							    *animals*)
						      #\M)
						     ((gethash (cons x y) *plants*) #\*)
						     (t #\space))))
		  (princ "|"))))

(defun evolution ()
  (draw-world)
  (fresh-line)
  (let ((str (read-line)))
    (cond ((equal str "quit") ())
	  (t (let ((x (parse-integer str :junk-allowed t)))
	     (if x
		 (loop for i below x ;; from 0 to x - 1
		       do (update-world)
		       if (zerop (mod i 1000))
			 do (princ #\.))
		 (update-world))
	     (evolution))))))
