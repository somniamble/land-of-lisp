					; empty is false

(if '()
    'this-is-true
    'this-is-false)

(if '(1)
    'this-is-true
    'this-is-false)

(defun my-length (lst)
  (if lst
      (1+ (my-length (cdr lst)))
      0))

(my-length '(list with four symbols)) ; 4

; use C-M-x to evaluate defun

; use C-c C-k to compile and load current buffer

; use C-c C-c to compile and load top level expression under point

; use C-c C-l to compile and load a file

; nil evaluates to itself and allows us to omit the '
(eq '() nil)  ; ==> T

; () is an empty form, and is parsed as an empty list
(eq '() ())   ; ==> T

; () and nil are to be treated the same, according to the lisp compiler
(eq '() 'nil) ; =

(if (= (+ 1 2) 3)
    (print "1 + 2 = 3")
    (print "math is broken, go to bed"))

(if (= (+ 1 2) 4)
    (print "1 + 2 = 4 ... ?")
    (print "1 + 2 != 4"))

; if is a _special form_, which gives it special privileges
(if (oddp 5)
    (print "5 is an odd number")
    (/ 1 0)) ; haha :)

(defvar *number-was-odd* nil)
(if (oddp 5)
    (progn ; progn evaluates everything passed to it and returns the result of the last
      (print "doing some progn stuff")
      (setf *number-was-odd* t)
      (print "number was odd")) 
    (print "number was even"))

;; basically a one-sided if with an implicit progn
;; when the condition is false, it returns nil and does nothing
(when (oddp 5)
  (setf *number-was-odd* t)
  (print "number is odd"))

;; this is basically a (not) wrapped around the condition of when
(unless (oddp 4)
  (setf *number-was-odd* nil)
  (print "number is even"))


;; here we've got cond, aka big dick bazuso
;;(defvar *arch-enemy* nil)
;;(defun pudding-eater (person)
;;  ;; cond has an implicit progn
;;  ;; conditions are evaluated in order, top down, until one of them fires off
;;  (cond ((eq person 'henry) (setf *arch-enemy* 'stupid-lisp-alien) 
;;	 (print "curse you lisp alien - you ate my pudding!"))
;;	((eq person 'jonny) (setf *arch-enemy* 'useless-ol-jonny)
;;	 (print "I hope you choke on my pudding jonny"))
;;	(t (print "why'd you eat my pudding for stranger?"))))
;; this is what the above actually macroexpands to:
;;
;;  (IF (EQ PERSON 'HENRY)
;;      (PROGN
;;       (SETF *ARCH-ENEMY* 'STUPID-LISP-ALIEN)
;;       (PRINT "curse you lisp alien - you ate my pudding!"))
;;      (IF (EQ PERSON 'JONNY)
;;          (PROGN
;;           (SETF *ARCH-ENEMY* 'USELESS-OL-JONNY)
;;           (PRINT "I hope you choke on my pudding jonny"))
;;          (THE T (PRINT "why'd you eat my pudding for stranger?"))))


;;(pudding-eater 'jonny)
;;*arch-enemy*
;;
;;(pudding-eater 'george-clooney)

;; and here's the equivalent using case rather than switch
(defvar *arch-enemy* nil)
(defun pudding-eater (person)
  (case person
    ((henry)   (setf *arch-enemy* 'stupid-lisp-alien) 
	       (print "curse you lisp alien - you ate my pudding!"))
    ((jonny)   (setf *arch-enemy* 'useless-ol-jonny)
	       (print "I hope you choke on my pudding jonny"))
    (otherwise (print "why'd you eat my pudding for stranger?"))))
;; here's what that case macro-expands to:
;; (LET ((#:G631 PERSON))
;;   (DECLARE (IGNORABLE #:G631))
;;   (COND
;;    ((OR (EQL #:G631 'HENRY)) NIL (SETF *ARCH-ENEMY* 'STUPID-LISP-ALIEN)
;;     (PRINT "curse you lisp alien - you ate my pudding!"))
;;    ((OR (EQL #:G631 'JONNY)) NIL (SETF *ARCH-ENEMY* 'USELESS-OL-JONNY)
;;     (PRINT "I hope you choke on my pudding jonny"))
;;    (T NIL (PRINT "why'd you eat my pudding for stranger?"))))

(and (oddp 5) (oddp 7) (oddp 9)) ;; ==> T
;; (mapcar #'and (list 5 7 9)) ; this does not work: "and is a macro, not a function." ; Oops!
(or (oddp 4) (oddp 5) (oddp 8))  ;; ==> T

;; we can do some tricky shit with or/and
(defparameter *is-it-even* nil)
;;(or (oddp 4) (setf *is-it-even* t)) ;; ==> T, the setf evaluates to T
(or (oddp 5) (setf *is-it-even* t)) ;; ==> does not evaluate the setf,
                                    ;;     because (oddp 5) is true, and `or` shortcircuits
*is-it-even* ;; ==> NIL

;; Lisp uses _shortcut Boolean evaluation_
;; suppose whe want to save a file to disk in the case that:
;;   the file was modified
;;   the user wishes to save the file

;;;; one way of achieving this:
;;(if *file-modified*
;;    (if (ask-user-about-saving)
;;	(save-file)))

;; another, perhaps more concise way:
;; (and *file-modified* (ask-user-about-saving) (save-file))

;; I do this sort of thing all the time in shell scripting.
;; e.g. [[ -f "$file" ]] && echo "file exists"
;; or   [[ -f "$file" ]] || echo "file does not exist"

;;;; a different way to express this, where the bools are used more explicitly:
;; (if (and *file-modified*
;; 	 (ask-user-about-saving))
;;    (save-file))

;; Member returns the sub-list starting with the provided `item`
(member 1 '(3 4 1 5)) ; ==> (1 5)

;; all non-nil values in lisp evaluate to true!
(if (member 1 '(3 4 1 5))
    (print "one is in the list")
    (print "one is not in the list"))

(member 2 '(3 4 1 5))

;; and nil evaluates to false !
(if (member 2 '(3 4 1 5))
    (print "two is in the list")
    (print "two is not in the list"))

;; here we see the first example of a function being passed, using #' notation
(find-if #'oddp '(2 4 5 6)) ;; ==> 5

(if (find-if #'oddp '(2 4 5 6))
    (print "there is an odd number")
    (print "there are no odd numbers"))


(find-if #'null '(2 3 nil 6)) ;; ==> nil, because it returns the value which makes #'null evaluate to T...

;; Conrad's rule of thumb for comparing stuff:
;; 1. USE `EQ` TO COMPARE SYMBOLS
;; 2. USE `EQUAL` FOR EVERYTHING ELSE

(defparameter *fruit* 'apple)

; simplest of lisp comparison functions, very fast, doesn't work for much other than symbols
(cond ((eq *fruit* 'apple) (print "this is an apple"))
      ((eq *fruit* 'orange) (print "this is an orange"))

;; we can use `equal` to compare basically any of the lisp primitive types

;; compare symbols
(equal 'apple 'apple)  ;; => T

(equal (list 1 2 3) (list 1 2 3)) ;; => T

(equal (list 1 2 3) (cons 1 (cons 2 ( cons 3 ())))) ;; => T

;; compare ints
(equal 5 5) ;; => T

;; compare floats
(equal 2.5 2.5) ;; => T

;; compare strings
(equal "foo" "foo") ;; => T

;; compare chars
(equal #\a #\a)  ;; => T

;; don't use it to compare ints to floats unless you want it to give you nil
(equal 2 2.0) ;; => Nil


;;;; `eql` ;;;;
;; `eql` is similar to `eq`, but it can handle comparison of numbers and characters

(eql 'foo 'foo) ;; => T

(eql 3.4 3.4) ;; => T

(eql #\a #\a) ;; => T

(eql (list 1 2 3) (list 1 2 3)) ;; => NIL

(eql "foo" "foo") ;; => NIL

;;;; `equalp` ;;;;
;; similar to equal, but it matches strings with different capitalization and
;; it can compare ints to floats

;; case insensitive string compare
(equalp "Bot Smith" "bot smith") ;; => T

;; compare ints to floats
(equalp 1 1.0) ;; => T
