;; different ways to print shit

;; starts a newline and adds a space after
(print "foo")

;; no newline and no space
(progn (prin1 "this")
       (prin1 "is")
       (prin1 "a")
       (prin1 "test"))

(defun say-hello ()
  (print "Please to type your name:")
  (let ((name (read)))
    (print "Nice to meet you, ")
    (prin1 name)))

;; (say-hello)

;;
;; CL-USER> (say-hello)
;; "Please to type your name:" somni
;; 
;; "Nice to meet you, " SOMNI 
;; SOMNI 

;; always ask yrself if read and print will be sufficient for what you're doing

(defun add-five ()
  (print "please enter a number:")
  (let ((num (read)))
    (print "When I add five I get:")
    (prin1 (+ num 5))))


;; important symbols for important use:
;; #\newline
;; #\space
;; #\tab

(print '3)     ;; int 
(print '3.4)   ;; float
(print 'foo)   ;; symbol
(print '"foo") ;; string
(print '#\a)   ;; char

;; read is basically the inverse of print

(princ '3)     ;; 3   ;; int 
(princ '3.4)   ;; 3.4 ;; float
(princ 'foo)   ;; FOO ;; symbol
(princ '"foo") ;; foo ;; string
(princ '#\a)   ;; a   ;; char

;; functions     | printing | reading   |
;;               -----------|---------- |
;; for computers |  print   |    read   |
;; for humans    |  princ   | read-line |

(defun improved-say-hello ()
  (print "Please to type your name:")
  (let ((name (read-line)))
    (princ "Nice to meet you, ")
    (princ name)))

;; '(+ 1 2) ; data mode
;; (+ 1 2)  ; code mode


(defparameter *foo* '(+ 1 2 3))
(eval *foo*)  ;; => 6

