A
#! sbcl --script

; took this from a stack overflow
; https://stackoverflow.com/questions/35187392/how-do-you-print-two-items-on-the-same-line-in-lisp
(defun printem (&rest args)
    (format t "~{~a~^ ~}~%" args))

(defun sep () (format t "~%---------------------------------------------------------------------~%~%"))

(defun square (n)
  (* n n))

(printem "some semantic considerations:")

(printem "
    let (<variable declarations>)
    ... body ...
          ")
(printem "
    example:
         (let ((a 5)
               (b 6))
           (+ a b))
         :" (let ((a 5) (b 6)) (+ a b)))

(printem "
    flet ((<function_name (arguments)>
              ...function body...))
      ...body...
         ")
(printem "
    examples:
         (flet ((f (n)
                   (+ n 10)))
           (f 5))
         :" (flet ((f (n) (+ n 10))) 5))

(printem "
         (flet ((f (n)
                   (+ n 10))
                (g (n)
                   (- n 3)))
           (g (f 5)))
         :" (flet ((f (n) (+ n 10)) (g (n) (- n 3))) (g (f 5))))

(printem "
    (labels ((<function_name (arguments)> ; can do multiple declarations
                ...function body...))     ; functions may reference themselves or other functions in the list
      ...body...) 
         ")

(printem "
         (labels ((a (n)
                     (+ n 5))
                  (b (n)
                     (+ (a n) 6)))
           (b 10))
         :" (labels ((a (n) (+ n 5)) (b (n) (+ (a n) 6))) (b 10)))
(sep)

(printem "basic math considerations")
(printem "square of ten:" (square 10))

(printem "'fooo equals 'FoOo?:" (eq 'fooo 'FoOo))

(printem "1 + 1.0 makes:" (+ 1 1.0))

(printem "53 to the 53rd power is:" (expt 53 53))

(printem "divide two integers 4 and 6 and get a fraction:" (/ 4 6))

(printem "divide with a floating point 4.0 and 6 and get a float:" (/ 4.0 6))

(sep)

(printem "use princ to print a string, while also yielding the value of the string!" (princ "Tutti Frutti"))

(printem "princ will also handle escape chars.")

(printem "evaluating expression (expt 2 3) \"code mode\":" (expt 2 3))

(printem "evaluating expression '(expt 2 3) \"data mode \":" '(expt 2 3))

(sep)

(printem "consing 'chicken onto 'nil:" (cons 'chicken 'nil) "is the same as ")
(printem "consing 'chicken onto ():" (cons 'chicken ()))
(printem "here is a list made by (cons 'chicken '(cat))" (cons 'chicken '(cat)))
(printem "here is a pair made by (cons 'chicken 'cat):" (cons 'chicken 'cat))
(printem "the pair is NOT a list proper")

(sep)

(printem "here are some equivalencies")
(printem "(cons 'pork (cons 'beef (cons 'chicken ()))):" (cons 'pork (cons 'beef (cons 'chicken ()))))
(printem "(list 'pork 'beef 'chicken)                 :" (list 'pork 'beef 'chicken))
(printem "'(pork beef chicken)                        :" '(pork beef chicken))

(sep)
