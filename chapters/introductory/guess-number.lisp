(defun average (x y) 
  (ash (+ x y) -1)) ; ash is binary shify, often used for binary search
; set global parameter
(defparameter *smallest* 1)

(defparameter *largest* 100)

(defun guess-my-number ()
  (average *smallest* *largest*))

(defun smaller ()
  (setf *largest* (1- (average *smallest* *largest*)))
  (guess-my-number))

; setf overwrites global parameters
(defun larger ()
  (setf *smallest* (1+ (average *smallest* *largest*)))
  (guess-my-number))

(defun start-over ()
  (defparameter *smallest* 1)
  (defparameter *largest* 100)
  (guess-my-number))
