;; starting off with some shenanigans

(setf *print-circle* t)

;; you can make a circular list! fuck you if you do this
(defparameter foo '(1 2 3))
(setf (cdddr foo) foo)

;; association lists, or alists
;; alists are very convenient, but they are not very efficient because of their O(n) lookup time
(defparameter *drink-order* '((somni . |boozy drip|)
			      (bill  . |double espresso|)
			      (lisa  . |small drip|)
			      (john  . |medium latte|)))
(assoc 'lisa *drink-order*) ;; ==> (lisa  . |small drip|)
(push '(lisa . |double shot dirty chai with soy milk|) *drink-order*)
(assoc 'lisa *drink-order*) ;; ==> (lisa . |double shot dirty chai with soy milk|)


;; visualizing tree-like data

(defparameter *house* '((walls
			  (mortar
			   (cement)
			   (water)
			   (sand))
			  (bricks))
			 (windows
			  (glass)
			  (frame)
			  (curtains))
			 (roof (shingles)
			  (chimney))))
