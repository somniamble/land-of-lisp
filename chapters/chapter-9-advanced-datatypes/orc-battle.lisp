;; fervor stuff
(defparameter *fervor-max* 30)
(defparameter *high-fervor* 20)
(defparameter *moderate-fervor* 15)
(defparameter *low-fervor* 5)
(defparameter *player-god* "Golgotha")
(defparameter *pray-cost* 3)

(defun apply-random-prayer ()
  (funcall (cdr (nth (random (length *prayer-effects*)) *prayer-effects*))))


  

;; player stats
(defparameter *player-health* nil)
(defparameter *player-agility* nil)
(defparameter *player-strength* nil)
(defparameter *player-fervor* nil)

(defparameter *prayer-effects*
  (list (cons 'heal (lambda ()
		    (printem "You are warmed and comforted by the light of" *player-god*)
		    (incf *player-health* (randval 20))))
	(cons 'bolster (lambda ()
		    (printem "You are bolstered by the power of" *player-god*)
		    (incf *player-strength* (randval 20))))
	(cons 'quicken (lambda ()
		    (printem "You are quickened by the guidance of" *player-god*)
		    (incf *player-agility* (randval 20))))))

;; monster stuff
(defparameter *monsters* nil)
(defparameter *monster-builders* nil)
(defparameter *monster-num* 12)

;;(defstruct monster (health 5))) ;; we lcan 
(defstruct monster (health (randval 10))) ;; we can supply a form which, when evaluated, gives the default value

(defun printem (msg var)
  (format t "~a ~a~%" msg var))

(defun fervor-msg ()
  (cond ((equal *player-fervor* *fervor-max*)
	 (concatenate 'string "You burn furiously with the harsh light of " *player-god*))
	((>= *player-fervor* *high-fervor*)
	 (concatenate 'string "You are wreathed in the cleansing light of " *player-god*))
	((>= *player-fervor* *moderate-fervor*)
	 (concatenate 'string "You feel the favor of " *player-god*))
	((>= *player-fervor* *low-fervor*)
	 (concatenate 'string "Your connection to " *player-god* " has dimmed"))
	((> *player-fervor* 0)
	 "Your flame flickers, a candle in the wind")
	(t "You feel the total absence of god")))


(defun orc-battle ()
  (init-monsters)
  (init-player)
  (game-loop) ;; game loop goes forever here, then we check game state at the end
  (when (player-dead)
    (princ "You have died, your kingdom was pillaged, and your bloodline was annihilated"))
  (when (monsters-dead)
    (princ "You have vanguished all of your foes, and your bloodline shall continue for a thousand thousand generations")))
      
;; empty defs here

(defun game-loop ()
  (unless (or (player-dead) (monsters-dead))
    (show-player)
    (dotimes (k (1+ (truncate (/ (max 0 *player-agility*) 15))))
      (unless (monsters-dead)
	(show-monsters)
	(player-action)))
    (fresh-line)
    (map 'list
	 (lambda (m)
	   (or (monster-dead m) (monster-attack m)))
	 *monsters*)
    (game-loop))) ;; recursive game-loop call

;;(dotimes (i 3)
;;  (fresh-line)
;;  (princ i)
;;  (princ ". suh dude~"))

;; ==> 0. suh dude~
;;     1. suh dude~
;;     2. suh dude~

(defun init-player ()
  (setf *player-health* 30)
  (setf *player-agility* 30)
  (setf *player-strength* 30)
  (setf *player-fervor* (- *fervor-max* 10)))

(defun player-dead ()
  (<= *player-health* 0))

(defun show-player ()
  (fresh-line)
  (printem "You are a noble paladin with a health of" *player-health*)
  (printem "an agility of" *player-agility*)
  (printem "and strength of " *player-strength*)
  (princ (fervor-msg)))

(defun randval (n)
  (1+ (random (max 1 n))))

(defun player-action ()
  (fresh-line)
  (princ "Action type: [s]tab, [d]ouble swing, [r]oundhouse, [p]ray, [c]hant:")
  (case (read)
    (s (monster-hit (pick-monster)
		    (+ 2 (randval (ash *player-strength* -1))))) ;; attack strength in range [3, strength / 2]
    (d (let ((x (randval (truncate (/ *player-strength* 6))))) ;; attack strength in range [1, strength / 6]
	 (printem "Your double swing has a strength of" x)
	 (monster-hit (pick-monster) x)
	 (unless (monsters-dead)
	   (monster-hit (pick-monster) x))))
    (r (dotimes (x (1+ (randval (truncate (/ *player-strength* 3))))) ;; attack  1 + strength / 3 times
	 (unless (monsters-dead)
	   (monster-hit (random-monster) 1))))
    (p (if (>= *player-fervor* *pray-cost*)
	     (progn (decf *player-fervor* *pray-cost*)
		    (apply-random-prayer))
	     (princ "Your words echo hollow into the distance...")))
    (otherwise (let ((fervor (randval 5)))
		 (princ "you chant this turn, gaining a little fervor")
		 (setf *player-fervor* (min *fervor-max* (+ *player-fervor* fervor)))))))

(defun random-monster ()
  (let ((m (aref *monsters* (random (length *monsters*)))))
    (if (monster-dead m)
	(random-monster) ;; if this monster is dead, call random monster again
	m))) ;; this monster is alive! return it.

;; making use of recursion here to call the function again if the player (me) fucks up
(defun pick-monster ()
  (fresh-line)
  (princ "Monster #:")
  (let ((x (read)))
    (if (not (and (integerp x) (>= x 1) (<= x *monster-num*)))
	(progn (princ "That is not a valid monster")
	       (pick-monster))
	(let ((m (aref *monsters* (1- x))))
	  (if (monster-dead m)
	      (progn (princ "That monster is already dead.")
		     (pick-monster))
	      m)))))

(defun init-monsters ()
  (setf *monsters*
	(map 'vector
	     (lambda (x)
	       (funcall (nth (random (length *monster-builders*))
			     *monster-builders*)))
	     (make-array *monster-num*))))

(defun monster-dead (m)
  (<= (monster-health m) 0))

(defun monsters-dead ()
  (every #'monster-dead *monsters*))

(defun show-monsters ()
  (fresh-line)
  (princ "Your foes:")
  (let ((x 0))
    (map 'list
	 (lambda (m)
	   (fresh-line)
	   (princ "   ")
	   (princ (incf x))
	   (princ ". ")
	   (if (monster-dead m)
	       (princ "**dead**")
	       (progn (princ "Health=")
		      (princ (monster-health m))
		      (princ ", ")
		      (monster-show m))))
    *monsters*)))


(defmethod monster-hit (m x)
  (decf (monster-health m) x)
  (if (monster-dead m)
      (progn (princ "You killed the ")
	     (princ (type-of m))
	     (princ "! "))
      (progn (princ "You hit the ")
	     (princ (type-of m))
	     (princ ", knocking off ")
	     (princ x)
	     (princ " health points! "))))

(defmethod monster-show (m)
  (princ "A fierce ")
  (princ (type-of m)))

(defmethod monster-attack (m))

;; monster orc

(defstruct (orc (:include monster)) (club-level (randval 8)))
(push #'make-orc *monster-builders*)
(defmethod monster-show ((m orc))
  (princ "a wicked orc with a level ")
  (princ (orc-club-level m))
  (princ " club"))

(defmethod monster-attack ((m orc))
  (let ((x (randval (orc-club-level m))))
    (princ "An orc swings his club at you and knocks off ")
    (princ x)
    (princ " of your health points. ")
    (decf *player-health* x))) ;; actually do the decrement here.

;; monster hydra
(defstruct (hydra (:include monster)))
(push #'make-hydra *monster-builders*)

(defmethod monster-show ((m hydra))
  (princ "A malicious hydra with ")
  (princ (monster-health m))
  (princ " heads."))

(defmethod monster-hit ((m hydra) x)
  (decf (monster-health m) x)
  (if (monster-dead m)
      (princ "The corpse of the fullt decapitated and decapacitated hydra collapses to the ground!")
      (progn (princ "You lop off ")
	     (princ x)
	     (princ " of the hydra's heads!"))))

(defmethod monster-attack ((m hydra))
  (let ((x (randval (ash (monster-health m) -1))))
    (princ "A hydra attacks you with ")
    (princ x)
    (princ " of its heads! It also grows one more head! ")
    (incf (monster-health m))
    (decf *player-health* x)))

;; monster slime mold
(defstruct (slime-mold (:include monster)) (sliminess (randval 5)))
(push #'make-slime-mold *monster-builders*)

(defmethod monster-show ((m slime-mold))
  (princ "A slime mold with a sliminess of ")
  (princ (slime-mold-sliminess m)))

(defmethod monster-attack ((m slime-mold))
  (let ((x (randval (slime-mold-sliminess m))))
    (princ "A slime mold wraps around your legs and decreases your agility by ")
    (princ x)
    (princ "! ")
    (decf *player-agility*)
    (when (zerop (random 2))
      (princ "It also squirts your face, taking away one healthpoint! ")
      (decf *player-health*))))

;; monster brigand

(defstruct (brigand (:include monster)))
(push #'make-brigand *monster-builders*)

(defmethod monster-attack ((m brigand))
  (let ((x (max *player-health* *player-agility* *player-strength*)))
    (cond ((= x *player-health*)
	   (princ "A brigand hits you with his slingshot, taking off 2 health! ")
	   (decf *player-health* 2))
	  ((= x *player-agility*)
	   (princ "A brigand catches you with his whip, taking off 2 agility! ")
	   (decf *player-agility* 2))
	  ((= x *player-strength*)
	   (princ "A brigand cuts your arm with his whip, taking off 2 strength! ")
	   (decf *player-strength*)))))
