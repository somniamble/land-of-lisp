;; FROM CHAPTER 9: ADVANCED DATATYPES AND GENERIC PROGRAMMING

;; arrays! constant-time element access.
(defparameter arr (make-array 3))
;; ==>  #(0 0 0 )

;; aref. like `ARray REF`
(aref arr 1)
;; ==> 0

(setf (aref arr 1) 'foo)
;; arr is now '#(0 foo 0)

;; why does this work? through the magic of Generic Setters...
;; The basic idea is,
;; PULLING something OUT of a datastructure can be reversed to
;; PUTTING something INTO a datastructure, it's basically the same operation but backwards.
;; well, on a conceptual level of course....

;;consider:
(defparameter lst '(a b c))
;; ==> (A B C)
(second lst)
;; ==> B
(setf (second lst) 'z)
(print lst) ;; ==> (A Z C)

;; magical --- as it turns out, setf asks the question:
;; "where did the item in the first argument originate from?"
;; as it turns out, the value came from the second item in the list named foo, we setf the location, it is modified
;; "in fact, the first argument in setf is a special sublanguage of Common Lisp,
;;   called a _generalized reference_"


;; not every lisp command is a generalized reference, but u can get creepy with it

(defparameter foo (make-array 4))

(setf (aref foo 2) '(x y z))

(setf (car (aref foo 2)) (make-hash-table))

(setf (gethash 'zoink (car (aref foo 2))) 5)

(print foo)

;; hash tables

(defparameter hashy (make-hash-table))
(gethash 'yuh hashy)

(setf (gethash 'yuh hashy) 'rashy)
(gethash 'yuh hashy) ;; ==> 'rashy, T

(defun initialize-hash (alist)
  (let ((hash-table (make-hash-table)))
    (mapc (lambda (association)
	    (setf (gethash (car association) hash-table)
		  (cdr association)))
	  alist)
    hash-table))

(defparameter *drink-order*
  (initialize-hash
   '((bill . double-espresso)
     (lisa . small-drip-coffee)
     (john . medium-latte))))

(gethash 'lisa *drink-order*)
;; SMALL-DRIP-COFFEE

;; RETURNING MULTIPLE VALUES:
;; nutty stuff here:

(round 2.4)
;; 2 ,
;; 0.4

(defun foo ()
  (values 3 7))
(foo)
;; 3, 7

;; the second value returned is simply ignored. Get lost.
(+ (foo) 5) ;; ==> 8

;; if we need the second value, however, we can use `multiple-value-bind`
(multiple-value-bind (a b) (foo)
  (* a b)) ;; ==> 21

;; MAKING A STRUCT
