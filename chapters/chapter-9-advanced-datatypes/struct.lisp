
;; definining a Structure is as follows:
(defstruct person
  name
  age
  measurement
  favorite-color)

;; a `person` has four properties, or slots
;; name, age, measurement, favorite-color


(defparameter *bill* (make-person :name "Bill"
				  :age 27
				  :measurement 6.5
				  :favorite-color "red"))
(print *bill*)
;; ==> #S(PERSON :NAME "Bill" :AGE 27 :MEASUREMENT 6.5 :FAVORITE-COLOR "red") 

(person-measurement *bill*) ;; ==> 6.5
(setf (person-measurement *bill*) 7.1)
(person-measurement *bill*) ;; ==> 7.1

;; this does NOT work in sbcl
;;(defparameter *ben* #S(person :name "Ben" :age 26 :measurement 4.6 :favorite-color "green"))
;;
;;(person-favorite-color *ben*)

;; we could do this too....
(defun make-person- (name age measurement favorite-color)
  (list name age measurement favorite-color))

(defun person-age- (person)
  (cadr person))

;; but is this clearer to work with and use? maybe, but maybe not.
(defparameter *ben* (make-person- "Ben" 26 4.6 "green"))
(person-age- *ben*) ;; 26

;; structures are good for mutable code, due to the ease of access of their properties
