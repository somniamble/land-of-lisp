;; FROM CHAPTER 9: ADVANCED DATATYPES AND GENERIC PROGRAMMING
;; generic sequence operations
(length '(a b c)) ;; ==> 3

(length "suh dude") ;; ==> 8

(length (make-array 10)) ;; ==> 10

;; in this case, `length` is using type checking to perform the correct operation for each datum.
;; however, if we know we're working with a list and we must prioritize performance...

(list-length '(1 2 3 4 5 6 7 8 9 0)) ;; ==> 10

;; sequence functions for searching:

(find-if #'numberp '(a b 5 d)) ;; ==> 5
(find-if #'oddp '(2 4 6 8)) ;; ==> NIL

(count #\s "mississippi") ;; ==> 4
(count #\x "haha wow ;)") ;; ==> 0

(position #\5 "2spooky5me") ;; ==> 7
(position #\y "no stop that") ;; ==> NIL

;; `any` function
;; e.g. (foldl or) . map predicate
(some #'numberp '(a b 5 d)) ;; ==> T

;; `all` function
;; e.g. (foldl and) . map predicate
(every #'numberp '(a b 5 d)) ;; ==> NIL


;; SEQUENCE FUNCTIONS FOR ITERATING

(reduce #'+ '(3 4 6 5 2)) ;; ==> 20

;; make sure to use initial values for reductions
(reduce (lambda (best item)
	  (if (and (evenp item) (> item best))
	      item
	      best))
	'(7 4 6 5 2)
	:initial-value 0) ;; ==> 6

;; or suffer the wrath of gnon
(reduce (lambda (best item)
	  (if (and (evenp item) (> item best))
	      item
	      best))
	'(7 4 6 5 2)) ;; ==> 7 ,terrible, horribl,e its a bug get it out of here


;; because reduce is generic, we can use it to make other generic functions
(defun sum (lst)
  (reduce #'+ lst
	  :initial-value 0))

(sum '(1 2 3)) ;; ==> 6
(sum (make-array 5 :initial-contents '(1 2 3 4 5))) ;; ==> 15

;;(sum "this won't work") ;; ==> #\t is not of type number

;; I have terrible news:
;; reduce is left-associative, so it produces this abomination:
(reduce #'cons '(1 2 3 4 5)) ;; ==> ((((1 . 2) . 3) . 4). 5)
;; but soft; what light through yonder window breaks?
(reduce #'cons '(1 2 3 4 5)
	:from-end t ;; ==> builds the operation up from the back of the list, e.g. foldl
	:initial-value NIL) ;; ==> (1 2 3 4 5)

;; map -- generic version of mapcar (which is for lists, only)

(map 'list
     (lambda (x)
       (* x x))
     '(1 2 3 4 5)) ;; ==> (1 4 9 16 25)

;; TWO MORE IMPORTANT SEQUENCE FUNCTIONS

;; subseq -- extracts a subsequence of a function

(subseq "america" 2 6) ;; ==> "eric"

;; 0 indexed
(subseq "america" 0 2) ;; ==> "am"

;; errors out
;;(subseq "america" -1 3) ;; ==> -1 is not of type SB-INDEX

(subseq "america" 3) ;; ==> "rica" -- if no second argument is provided it goes to the end

;;(subseq "america" 10) ;; ==> 10 is out of bounds. "the indices 10 and NIL are bad for a sequence of length 7..."

(subseq "america" 1 1) ;; ==> "" -- gets the sequence starting and ending at 1. so, ""

;;(subseq "america" 5 2) ;; the indices 5 and 2 are bad for a sequence of length 7...


;; CREATING OUR OWN GENERICS WITH TYPE PREDICATES

(numberp 5) ;; ==> T

;; THE TYPE PREDICATES YOU WILL USE MOST FREQUENTLY:
;; #################################################
;; arrayp
;; characterp
;; consp
;; functionp
;; hash-table-p
;; listp
;; stringp
;; symbolp
;; #################################################

;; let's look at one way to write a generic `add` method,
;;(defun add (a b)
;;  (cond ((and (numberp a) (numberp b)) (+ a b))
;;	((and (listp a) (listp b)) (append a b))))
;;
;;(add 3 4) ;; ==> 7
;;
;;(add '(a b) '(c d)) ;; ==> (A B C D)

;; why wouldn't we want to do this? well:

;; SINGLE, MONOLITHIC FUNCTION FOR ALL DATATYPES
;; -- fine for two types, but what if we wanted to handle a dozen, or more? Ugly. Monstrous.
;;
;; MODIFICATIONS TO FUNCTION REQUIRED TO ACCOMMODATE NEW CASES.
;; -- we have to touch the function definition every time we add a new type
;; -- what if the method is in library 'a' and we would like to extend it in library 'b'?
;;
;; HARD TO READ:
;; -- what if im tired and i don't want to read a million lines of cond cases?
;;
;; PERFORMANCE ISSUES:
;; -- the compiler has a much harder time being certain that a nd b are lists here...
;;    so it will have a much harder time optimizing our code for us

;; generic `add` redux:

(defmethod add ((a number) (b number))
  (+ a b))

(defmethod add ((a list) (b list))
  (append a b))

(add 3 4) ;; 7

(add '(a b) '(c d)) ;; (A B C D)

;; wowee!
;; defmethod can also be used with defstruct.... you see where I'm going with this?
