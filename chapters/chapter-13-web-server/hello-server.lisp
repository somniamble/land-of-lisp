(load "server.lisp")

;; places the document within a proper http 200 response
(defun response (document)
  (format nil "HTTP/1.1 200 OK
Content-Length: ~d
Content-Type: text/html

~a" (length document) document))

;; wraps the document up in a proper html page
(defun wrap-html (page-title body)
  (format nil "<!DOCTYPE html>
<html>
<head>
<title>~a</title>
</head>
<body>
~a
</body>
</html>" page-title body))

;; our basic hello request handler
(defun hello-request-handler (path header params)
  (flet ((respond (title body)
		 (response (wrap-html title body))))
    (if (equal path "greeting")
	(let ((name (assoc 'name params)))
	    (if (not name)
		(respond "whoareyou?" "<form>What is your name?
    <input name='name' />
    </form>")
		(respond "hello" (format nil "<p>Nice to meet you, ~a!<p>" (cdr name)))))
	(respond ".info" (format nil "You have arrived at ~a with headers ~{<p>~a</p>~%~}" path header)))))

(serve #'hello-request-handler)
