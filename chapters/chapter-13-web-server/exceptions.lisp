
;; (error "foo")
;; *** foo
;; Condition of type SIMPLE ERROR

;; CUSTOM CONDITION , DEFINITION

;; Definiting a custom condition 
;; Creating a custom condition named 'fuck
;; when this condition is signaled, we supply a custom function that is called to report the rror
(define-condition fuck () ()                  
  (:report (lambda (condition stream) ;; here we declare the reporting function
	     (princ "Quit FUCKing around, donkass" stream)))) ;; and within that, we print a custom message
;; Macro Expands to:
;;
;;(PROGN
;; (SB-KERNEL::%DEFINE-CONDITION 'FUCK '(CONDITION)
;;                               (SB-KERNEL::FIND-CONDITION-LAYOUT 'FUCK
;;                                                                 '(CONDITION))
;;                               (LIST) (LIST) 'NIL 'NIL (SB-C:SOURCE-LOCATION))
;; (SB-KERNEL::%SET-CONDITION-REPORT 'FUCK
;;                                   #'(SB-INT:NAMED-LAMBDA (SB-KERNEL::CONDITION-REPORT
;;                                                           FUCK)
;;                                         (CONDITION STREAM)
;;                                       (DECLARE (TYPE CONDITION CONDITION)
;;                                                (TYPE STREAM STREAM))
;;                                       (FUNCALL
;;                                        #'(LAMBDA (CONDITION STREAM)
;;                                            (PRINC
;;                                             "Quit FUCKing around, donkass"
;;                                             STREAM))
;;                                        CONDITION STREAM)))
;; 'FUCK)

;; (error 'fuck)
;; *** Quit FUCKing around, donkass
;;     [Condition of type FUCK]
;;Backtrace:
;;  0: (SB-INT:SIMPLE-EVAL-IN-LEXENV (ERROR (QUOTE FUCK)) #<NULL-LEXENV>)
;;  1: (EVAL (ERROR (QUOTE FUCK)))
;; --more--

;; ERROR HANDLING
(defun fucky-wucky ()
  (error 'fuck))

(handler-case (fucky-wucky)
  (fuck () "i did a fucky!") 
  (uhoh () "Uh oh! Uh oh! Uh oh!"))
;; => "i did a fucky!"
;; Macro Expands To:
;;(SB-INT:DX-FLET ((#:FORM-FUN-631 ()
;;                   (PROGN (FUCKY-WUCKY)))
;;                 (#:FUN626 ()
;;                   (PROGN "i did a fucky!"))
;;                 (#:FUN628 ()
;;                   (PROGN "Uh oh! Uh oh! Uh oh!")))
;;  (DECLARE (OPTIMIZE (SB-C::CHECK-TAG-EXISTENCE 0)))
;;  (BLOCK #:BLOCK629
;;    (SB-INT:DX-LET ((#:CELL630 (CONS :CONDITION NIL)))
;;      (DECLARE (IGNORABLE #:CELL630))
;;      (TAGBODY
;;        (SB-KERNEL::%HANDLER-BIND
;;         ((FUCK
;;           (LAMBDA (SB-KERNEL::TEMP)
;;             (DECLARE (IGNORE SB-KERNEL::TEMP))
;;             (GO #:TAG625)))
;;          (UHOH
;;           (LAMBDA (SB-KERNEL::TEMP)
;;             (DECLARE (IGNORE SB-KERNEL::TEMP))
;;             (GO #:TAG627))))
;;         (RETURN-FROM #:BLOCK629 (#:FORM-FUN-631)))
;;       #:TAG625
;;        (RETURN-FROM #:BLOCK629 (#:FUN626))
;;       #:TAG627
;;        (RETURN-FROM #:BLOCK629 (#:FUN628))))))

;; error handling is arbitrary... you could do whatever here afterwards.
(handler-case (fucky-wucky)
  (fuck () (progn (princ "YOU SHOULD NOT HAVE DONE THAT")
		  "uwu oh no ~")) 
  (uhoh () "Uh oh! Uh oh! Uh oh!"))
;; => YOU SHOULD NOT HAVE DONE THAT "uwu oh no ~"

;; unwind-protect makes sure to evaluate whatever comes after the first argument even if it throws
(unwind-protect (/ 1 0)
  (princ "It is of the utmost importance that you hear this message"))
;;=> *** Lisp error during constant folding:
;;       Arithmetic error DIVISION-BY-ZERO signalled
;; It is of the utmost importance that you hear this message

(unwind-protect (/ 1 1)
  (princ "No problems here"))
;; => No problems here1
;; the unwind-protect is what the 'with-open-file macro leverages to close the file correctly


